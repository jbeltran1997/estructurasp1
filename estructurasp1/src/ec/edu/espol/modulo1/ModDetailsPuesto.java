/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author johnny
 */
public class ModDetailsPuesto {
    private TextField name;
    private TextField cedula;
    private TextField num;
    private Button modificar;
    private Button atras;
    private Pane root;

    public ModDetailsPuesto() {
        name = new TextField();
        cedula = new TextField();
        num = new TextField();
        modificar = new Button("Modificar");
        atras = new Button("Atras");
        root = new VBox(name, cedula, num, modificar, atras);
    }

    public TextField getName() {
        return name;
    }

    public TextField getCedula() {
        return cedula;
    }

    public TextField getNum() {
        return num;
    }

    public Button getModificar() {
        return modificar;
    }

    public Button getAtras() {
        return atras;
    }

    public Pane getRoot() {
        return root;
    }
    
    
    
}
