/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.ayudadores;

import ec.edu.espol.modulo2.Continente;
import ec.edu.espol.modulo2.Migrante;
import ec.edu.espol.modulo2.Pais;
import ec.edu.espol.modulo2.Provincia;
import ec.edu.espol.modulo2.Canton;
import ec.edu.espol.modulo2.RegistroMigratorio;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

/**
 *
 * @author Nicole Alvarez
 */
public class ManejoArchivo {
    
    public static void leerArchivo(){
        try(BufferedReader inputStream = new BufferedReader(new FileReader("src/archivos/migraciones.csv"));)
        { 
            String line = null;
            int ID_pro=0;
            String id;
            int count=0;
            while( (line = inputStream.readLine()) != null ){
                if(count!=0){
                id = String.format("%06d", ++ID_pro);
                String[] s = line.split(";");
                Pais ppro = new Pais(s[14],id);
                Pais pres = new Pais(s[15],id);
                Pais pa_registro = new Pais(s[13],id);
                Continente con_pro = new Continente(s[18],id);
                Continente con_re = new Continente(s[19],id);
                Continente con_na = new Continente(s[20],id);
                Continente subcon_pro = new Continente(s[21],id);
                Continente subcon_na = new Continente(s[22],id);
                Provincia pro_registro = new Provincia (s[3],id);
                Canton can_registro = new Canton(s[4],id);
                Migrante migrante = new Migrante(id, s[1], s[8], s[9], s[10], ppro, pres, con_pro, con_re, con_na, subcon_pro, subcon_na);
                //RegistroMigratorio reg =new RegistroMigratorio(s[0],s[2], s[5],s[6], s[7], s[11], s[12], migrante, pro_registro, can_registro, id, pa_registro);
                //Date d = new Date(s[10]);            
                }
                count++;
            }
        }
        catch(FileNotFoundException e)
        {
          System.out.println("File not found.");
        }
        catch(IOException e)
        {
           System.out.println("Error reading from file ");
        }
    
    }
    
   
    
}