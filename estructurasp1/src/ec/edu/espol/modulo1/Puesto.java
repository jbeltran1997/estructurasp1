package ec.edu.espol.modulo1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author johnny
 */
public class Puesto {
    public final static PuestoView view = new PuestoView();
    private Encargado encargado;
    private int num = 0;
    private static int count = 0;
    
    
    
    public Puesto(Encargado e){
        this.encargado = e;
        num = ++count;
    }
    public void atender(Turno t){
        
    }
    
    public int getNum(){
        return num;
    }

    public Encargado getEncargado() {
        return encargado;
    }

    public void setEncargado(Encargado encargado) {
        this.encargado = encargado;
    }

    @Override
    public String toString() {
        return String.valueOf(num);
    }

    public void setNum(int num) {
        this.num = num;
    }
    
    
}
