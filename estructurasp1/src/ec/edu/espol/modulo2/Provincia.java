/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo2;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Nicole Alvarez
 */
public class Provincia {
    private final String filename = "src/archivos/Provincia.txt";
    private String nombre_provincia,idpro;
    private LinkedList<Canton> cantones;
    private LinkedList<RegistroMigratorio> registro;

    public Provincia (String nombre_provincia,String idpro) {
        this.nombre_provincia = nombre_provincia;
        this.idpro =idpro;
        cantones = new LinkedList<>();
        registro = new LinkedList<>();
        
    }
    
    public Provincia (String nombre_provincia,LinkedList<Canton> cantones) {
        this.nombre_provincia = nombre_provincia;
        this.cantones = cantones;
        idpro=null;
    }
    
   public void agregarCanton(Canton c){
       if(c!=null&&!cantones.contains(c)){
           cantones.add(c);
       } 
   }
   
   public void escribirProvincia(){
       try(BufferedWriter outputStream =
                 new BufferedWriter(new FileWriter(filename,true)))
        {
                
                outputStream.write(nombre_provincia+","+idpro);
                outputStream.newLine();
                outputStream.close();
            
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error opening the file out.txt."+ e.getMessage());
        }
        catch(IOException e){
            System.out.println("IOException."+ e.getMessage());
        }
   
   }

    public String getNombre_provincia() {
        return nombre_provincia;
    }

    public void setNombre_provincia(String nombre_provincia) {
        this.nombre_provincia = nombre_provincia;
    }

    public String getIdpro() {
        return idpro;
    }

    public void setIdpro(String idpro) {
        this.idpro = idpro;
    }

    
   
   
   
   
    
    public LinkedList<Canton> getCantones() {
        return cantones;
    }

    public void setCantones(LinkedList<Canton> cantones) {
        this.cantones = cantones;
    }

   
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Provincia other = (Provincia) obj;
        if (!Objects.equals(this.nombre_provincia, other.nombre_provincia)) {
            return false;
        }
        if (!Objects.equals(this.idpro, other.idpro)) {
            return false;
        }
        return true;
    }

    public LinkedList<RegistroMigratorio> getRegistro() {
        return registro;
    }

    public void setRegistro(LinkedList<RegistroMigratorio> registro) {
        this.registro = registro;
    }

    

    @Override
    public String toString() {
        return nombre_provincia;
    }
    
    
    
    
}
