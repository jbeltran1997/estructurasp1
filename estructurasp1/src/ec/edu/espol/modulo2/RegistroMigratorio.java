/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


/**
 *
 * @author Nicole Alvarez
 */
public class RegistroMigratorio {
    private String filename = "src/archivos/RegistroMigratorio.txt";
    private String tipo_migracion,nacionalidad
            ,via_transporte,anio_movi,mes_movi,dia_movi,
            genero_mig,anio_nacimiento,ocu_migra,nacion_migran,clase_migra,motivo_viaje;
    private int edad_migra;
    private Pais pais_procmigra;
    private Pais pais_resmigra;
    private Migrante migrante;
    private Provincia prov_regmigra;
    private Canton cant_regmigra;
    private Pais nac_regi,lug_proc;
    private Continente cont_promi;
    private Continente cont_resmi;
    private Continente continen_nacmi;
    private Continente subcont_promi;
    private Continente suncont_resmi;
    private String id;
    private int ID_pro=0;
 

    public RegistroMigratorio(String id,String tipo_migracion, String via_transporte, 
            String anio_movi, String mes_movi, String dia_movi, String clase_migra, 
            String motivo_viaje, Migrante migrante, Provincia prov_regmigra,Canton cant_regmigra,Pais nac_regi) {
        this.migrante = migrante;
        this.tipo_migracion = tipo_migracion;
        this.nacionalidad = migrante.getNacionalidad();
        this.via_transporte = via_transporte;
        this.prov_regmigra = prov_regmigra;
        this.cant_regmigra=cant_regmigra;
        this.anio_movi = anio_movi;
        this.mes_movi = mes_movi;
        this.dia_movi = dia_movi;
        this.genero_mig= migrante.getGenero();
        this.anio_nacimiento= migrante.getAnio_nacimiento();
        this.ocu_migra = migrante.getOcupacion();
        this.clase_migra = clase_migra;
        this.motivo_viaje = motivo_viaje;
        this.nac_regi = nac_regi;
        this.pais_procmigra = migrante.getLugarpro();
        this.lug_proc = migrante.getLugarpro();
        this.edad_migra=migrante.getEdad();
        this.cont_promi=migrante.getCont_pro();
        this.cont_resmi=migrante.getCont_res();
        this.continen_nacmi=migrante.getContinen_nac();
        this.subcont_promi=migrante.getSubcont_pro();
        this.suncont_resmi=migrante.getSuncont_res();
        this.id = id;
        
        
    }

 
    
    
    public void GuardarRegistro(){
        id = String.format("%01d", ++ID_pro);
       try(BufferedWriter outputStream =
                 new BufferedWriter(new FileWriter(filename,true)))
        {         
                outputStream.write(id+","+tipo_migracion+","+nacionalidad+","+via_transporte+","+
                        prov_regmigra+","+cant_regmigra+","+
                        anio_movi+","+mes_movi+","+dia_movi+","+genero_mig+","+anio_nacimiento+","+ocu_migra+","+clase_migra+","+motivo_viaje+","
                        +nac_regi+","+pais_procmigra+","+
                        lug_proc+","+edad_migra+","+cont_promi+","+cont_resmi+","+continen_nacmi+","+subcont_promi+","+suncont_resmi);
                outputStream.newLine();
                outputStream.close();
            
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error opening the file out.txt."+ e.getMessage());
        }
        catch(IOException e){
            System.out.println("IOException."+ e.getMessage());
        }
   
   }
    public void EliminarRegistro(){
       try(BufferedWriter outputStream =
                 new BufferedWriter(new FileWriter(filename,true)))
        {         
                outputStream.write("");
                outputStream.newLine();
                outputStream.close();
            
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error opening the file out.txt."+ e.getMessage());
        }
        catch(IOException e){
            System.out.println("IOException."+ e.getMessage());
        }
   
   }
    
    
    
    public static void  Vicular(Migrante migrante, RegistroMigratorio rm,
            Canton canton,Continente cn,Continente cp,Continente cr,Provincia p,Continente scp,Continente scr,Pais pa)
    {
        p.getCantones().add(canton);
        cr.getPaices().add(pa);
        pa.agregarProvincia(p);
        migrante.getRegistrContinenteRes().add(cr);
        migrante.getRegistroCantones().add(canton);
        migrante.getRegistroProvincia().add(p);
        migrante.getRegistroPaisRes().add(pa);
        pa.getRegistros().add(rm);
        migrante.getRegistro().add(rm);
        canton.getRegistro().add(rm);
        cp.getRegistrosCP().add(rm);
        cr.getRegistrosCR().add(rm);
        cr.getRegistrosCN().add(rm);
        p.getRegistro().add(rm);
        scp.getRegistrosSCP().add(rm);
        scr.getRegistrosSBR().add(rm);
         
    }

  

    public String getTipo_migracion() {
        return tipo_migracion;
    }

    public void setTipo_migracion(String tipo_migracion) {
        this.tipo_migracion = tipo_migracion;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getVia_transporte() {
        return via_transporte;
    }

    public void setVia_transporte(String via_transporte) {
        this.via_transporte = via_transporte;
    }

    public String getAnio_movi() {
        return anio_movi;
    }

    public void setAnio_movi(String anio_movi) {
        this.anio_movi = anio_movi;
    }

    public String getMes_movi() {
        return mes_movi;
    }

    public void setMes_movi(String mes_movi) {
        this.mes_movi = mes_movi;
    }

    public String getDia_movi() {
        return dia_movi;
    }

    public void setDia_movi(String dia_movi) {
        this.dia_movi = dia_movi;
    }

    public String getGenero_mig() {
        return genero_mig;
    }

    public void setGenero_mig(String genero_mig) {
        this.genero_mig = genero_mig;
    }

    public String getAnio_nacimiento() {
        return anio_nacimiento;
    }

    public void setAnio_nacimiento(String anio_nacimiento) {
        this.anio_nacimiento = anio_nacimiento;
    }

    public String getOcu_migra() {
        return ocu_migra;
    }

    public void setOcu_migra(String ocu_migra) {
        this.ocu_migra = ocu_migra;
    }

    public String getNacion_migran() {
        return nacion_migran;
    }

    public void setNacion_migran(String nacion_migran) {
        this.nacion_migran = nacion_migran;
    }

    public String getClase_migra() {
        return clase_migra;
    }

    public void setClase_migra(String clase_migra) {
        this.clase_migra = clase_migra;
    }

    public String getMotivo_viaje() {
        return motivo_viaje;
    }

    public void setMotivo_viaje(String motivo_viaje) {
        this.motivo_viaje = motivo_viaje;
    }

    public int getEdad_migra() {
        return edad_migra;
    }

    public void setEdad_migra(int edad_migra) {
        this.edad_migra = edad_migra;
    }

    public Pais getPais_procmigra() {
        return pais_procmigra;
    }

    public void setPais_procmigra(Pais pais_procmigra) {
        this.pais_procmigra = pais_procmigra;
    }

    public Pais getPais_resmigra() {
        return pais_resmigra;
    }

    public void setPais_resmigra(Pais pais_resmigra) {
        this.pais_resmigra = pais_resmigra;
    }

    public Pais getNac_regi() {
        return nac_regi;
    }

    public void setNac_regi(Pais nac_regi) {
        this.nac_regi = nac_regi;
    }

    public Pais getLug_proc() {
        return lug_proc;
    }

    public void setLug_proc(Pais lug_proc) {
        this.lug_proc = lug_proc;
    }

    public Continente getCont_promi() {
        return cont_promi;
    }

    public void setCont_promi(Continente cont_promi) {
        this.cont_promi = cont_promi;
    }

    public Continente getCont_resmi() {
        return cont_resmi;
    }

    public void setCont_resmi(Continente cont_resmi) {
        this.cont_resmi = cont_resmi;
    }

    public Continente getContinen_nacmi() {
        return continen_nacmi;
    }

    public void setContinen_nacmi(Continente continen_nacmi) {
        this.continen_nacmi = continen_nacmi;
    }

    public Continente getSubcont_promi() {
        return subcont_promi;
    }

    public void setSubcont_promi(Continente subcont_promi) {
        this.subcont_promi = subcont_promi;
    }

    public Continente getSuncont_resmi() {
        return suncont_resmi;
    }

    public void setSuncont_resmi(Continente suncont_resmi) {
        this.suncont_resmi = suncont_resmi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

  

    public Migrante getMigrante() {
        return migrante;
    }

    public void setMigrante(Migrante migrante) {
        this.migrante = migrante;
    }

    public Provincia getProvincia() {
        return prov_regmigra;
    }

    public void setProv_regmigra(Provincia prov_regmigra) {
        this.prov_regmigra = prov_regmigra;
    }
    

    public Canton getCant_regmigra() {
        return cant_regmigra;
    }

    public void setCant_regmigra(Canton cant_regmigra) {
        this.cant_regmigra = cant_regmigra;
    }

    @Override
    public String toString() {
        return "RegistroMigratorio{ id = " + id  + "tipo_migracion=" + tipo_migracion + ", "
                + "nacionalidad=" + nacionalidad + ", via_transporte=" + via_transporte + 
                ", anio_movi=" + anio_movi + ", mes_movi=" + mes_movi + ", dia_movi=" + dia_movi + 
                ", genero_mig=" + genero_mig + ", anio_nacimiento=" + anio_nacimiento + ", ocu_migra=" 
                + ocu_migra + ", nacion_migran=" + nacion_migran + ", clase_migra=" + clase_migra + ","
                + " motivo_viaje=" + motivo_viaje + ", edad_migra=" + edad_migra + ", pais_procmigra=" + 
                pais_procmigra + ", pais_resmigra=" + pais_resmigra + ", migrante=" + migrante + ", prov_regmigra=" + 
                prov_regmigra + ", cant_regmigra=" + cant_regmigra + ", nac_regi=" + nac_regi + ", cont_promi=" + 
                cont_promi + ", cont_resmi=" + cont_resmi + ", continen_nacmi=" + continen_nacmi + ", subcont_promi="
                + subcont_promi + ", suncont_resmi=" + suncont_resmi +'}';
    }

   
   
    
    
    
}
