/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import ec.edu.espol.main.Estructurasp1;
import ec.edu.espol.main.PaneOrganizerModulo2;

/**
 *
 * @author johnny
 */
public class PuestoController {
    private AtencionTurnosModel model;
    private PuestoView view;
    private PrincipalController controller;
    public PuestoController(AtencionTurnosModel model, PuestoView view, PrincipalController controller){
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.view.getAtenderButton().setOnAction(e->{
            Turno t = model.atender();
            Puesto p = this.model.getPuesto();
            this.controller.displayTurno(p, t);
            PaneOrganizerModulo2 pn = new PaneOrganizerModulo2();
            Estructurasp1.scene.setRoot(pn.getRoot());
        });
    }
    
    
    
    
}
