/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.main;

import ec.edu.espol.ayudadores.StageClass;
import ec.edu.espol.ayudadores.ValoresRegistro;
import ec.edu.espol.modulo2.Canton;
import ec.edu.espol.modulo2.Continente;
import ec.edu.espol.modulo2.ListaRegistrosMigratorios;
import ec.edu.espol.modulo2.Migrante;
import ec.edu.espol.modulo2.Pais;
import ec.edu.espol.modulo2.Provincia;
import ec.edu.espol.modulo2.RegistroMigratorio;
import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.function.UnaryOperator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author Nicole Alvarez
 */
public class PaneOrganizerModulo2 {
    private String[] fecha = new String[3];
    public static ListaRegistrosMigratorios listado;
    private VBox root;
    private DatePicker date;
    GridPane contDatos;
    String[] datosPersona;
    ValoresRegistro v;
    private Background Bg;
    private StageClass stage;
    private int ID_pro=0;
    private String id;
    private TextField tx;
    
    
    public PaneOrganizerModulo2(){
        Label lb = new Label("REGISTRO MIGRATORIO");
        lb.setFont(new Font("Berlin Sans FB", 24));
        listado = new ListaRegistrosMigratorios();
        BorderPane bp= new BorderPane();
        root = new VBox();
        contDatos = new GridPane();
        datosPersona = new String[23];
        v = new ValoresRegistro();
        tx = new TextField();
        tipoEntrada();tipoNacionalidad();viaTransporte();procanRegistroMigrante();
        //fechaMigracion();
        fechar();
        sexoMigrante();
        anioNacimientoMigrante();ocupacionMigrante();claseMigratoria();
        motivoViaje();contPaiNacionaliad();contPaProcedencia();
        contPaRecidencia();lugarProcedencia();
        bp.setCenter(lb);
        bp.setPadding(new Insets(20, 20, 20, 20));
        root.getChildren().addAll(bp,contDatos);
        backgroundStage();
        initbottons();
       
        
        
        
    }
    
    private void backgroundStage(){
       Image img = new Image("Imagenes/2.jpg") ;
       stage = new StageClass(root,img);
       stage.backgroundStage(Bg);

 
    }
    
    public Pane getRoot(){
        return root;
    }
    
    private void initbottons(){
    Button guardar = new Button("Guardar");
    
    guardar.setOnAction((e)->{
       //String fechas = String.valueOf(date.getValue());
        //fecha = fechas.split("-");
        String texto = tx.getText();
       if(texto.equals("")){
           datosPersona[10]="Sin ESpecificar";
       }
       datosPersona[10]=texto;
        guardar();
        PaneTable  pT= new PaneTable();
        Estructurasp1.scene.setRoot(pT.getRoot());
        
    });
    
    contDatos.add(guardar, 3, 7);

    }
    
    
    
    private void guardar(){
        Label mensaje1 = new Label();
        mensaje1.setStyle("-fx-text-fill: blue;");
        contDatos.add(mensaje1, 2, 7);
        id = String.format("%01d", ++ID_pro);
        Pais ppro = new Pais(datosPersona[14],id);
        Pais pres = new Pais(datosPersona[15],id);
        Pais pa_registro = new Pais(datosPersona[13],id);
        Continente con_pro = new Continente(datosPersona[18],id);
        Continente con_re = new Continente(datosPersona[19],id);
        Continente con_na = new Continente(datosPersona[20],id);
        Continente subcon_pro = new Continente(datosPersona[21],id);
        Continente subcon_na = new Continente(datosPersona[22],id);
        Provincia pro_registro = new Provincia (datosPersona[3],id);
        Canton can_registro = new Canton(datosPersona[4],id);
        Migrante migrante = new Migrante(id, datosPersona[1], datosPersona[8], datosPersona[9],
                datosPersona[10], ppro, pres, con_pro, 
                con_re, con_na, subcon_pro, subcon_na);
        RegistroMigratorio reg =new RegistroMigratorio(id,datosPersona[0],datosPersona[2], datosPersona[5],datosPersona[6], datosPersona[7], datosPersona[11], 
                datosPersona[12], migrante, pro_registro, can_registro, pa_registro); 
        int cont =0;
        try{
        fechar();
        listado.anadirRegistro(reg);
        }catch(Exception e){
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Registro Incompleto");
            alert.setHeaderText(null);
            alert.setContentText("Los datos estan incompletos por favor modifique o vuelva a registrar");
            alert.showAndWait();
            cont =1;
          
            
        }
            if(cont!=1){
            RegistroMigratorio.Vicular(migrante, reg, can_registro, con_na, con_pro, con_re, pro_registro, subcon_pro, subcon_na, pres); 
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Guardado ");
            alert.setHeaderText(null);
            alert.setContentText("GUARDADO CON EXITO");
            alert.showAndWait();
            }
        
           
    }
    

    
   
    
    private void tipoEntrada(){
        Label lm = new Label("Elija el tipo de movimiento:  ");
        lm.setFont(new Font("Berlin Sans FB", 15));
        HBox mov = new HBox();
        ArrayList<String> ar = new ArrayList<>();
        ar.add("Entrada");
        ar.add("Salida");
       ComboBox<String> cblocal = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cblocal.setItems(lista);
                    cblocal.setOnAction((e)->{ 
                        String tm = cblocal.getSelectionModel().getSelectedItem();
                        datosPersona[0]=tm;
                    });
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm,cblocal);
       contDatos.add(mov, 0, 0);
       
       
       
       

   }
    
    private void tipoNacionalidad(){
        Label lm = new Label("Elija nacionalidad:  ");
        lm.setFont(new Font("Berlin Sans FB", 15));
        HBox mov = new HBox();
        ArrayList<String> ar = new ArrayList<>();
        ar.add("Ecuatorianos");
        ar.add("Extranjeros");
       ComboBox<String> cblocal = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cblocal.setItems(lista);
                    cblocal.setOnAction((e)->{ 
                        String tn = cblocal.getSelectionModel().getSelectedItem();
                        datosPersona[1]=tn;
                    });
                    
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm,cblocal);
       contDatos.add(mov, 0, 1);
       
       
   }
    
    private void viaTransporte(){
        Label lm = new Label("Elija vía de Transporte:  ");
        lm.setFont(new Font("Berlin Sans FB", 15));
        HBox mov = new HBox();
        ArrayList<String> ar = new ArrayList<>();
        ar.add("Via Aérea");
        ar.add("Via Terrestre");
        ar.add("Via Marítima");
       ComboBox<String> cblocal = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cblocal.setItems(lista);
                    cblocal.setOnAction((e)->{ 
                        String tn = cblocal.getSelectionModel().getSelectedItem();
                        datosPersona[2]=tn;
                    });
                    
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm,cblocal);
       contDatos.add(mov, 0, 2);
       
       
   }
    
   private void procanRegistroMigrante(){
       Label lm = new Label("Elija Provincia Actual:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       HBox mov = new HBox();
       Label lm2 = new Label("Elija Canton:  ");
       lm2.setFont(new Font("Berlin Sans FB", 15));
       HBox mov2 = new HBox();
       ArrayList<String> ar = ValoresRegistro.Provincias();
       ComboBox<String> cton = new ComboBox<>();
       ComboBox<String> cbprov = new ComboBox<>();
       ObservableList<String> listap = FXCollections.observableArrayList(ar);
                    cbprov.setItems(listap);
                    cbprov.setOnAction((e)->{ 
                        String tn = cbprov.getSelectionModel().getSelectedItem();
                        ArrayList<String> r2 = ValoresRegistro.asignarCanton(tn);
                        ObservableList<String> listac = FXCollections.observableArrayList(r2);
                        cton.setItems(listac);
                        datosPersona[3]=tn;
                    });
                    mov2.setPadding(new Insets(15, 15, 15, 15));
                    mov2.getChildren().addAll(lm,cbprov);
                    contDatos.add(mov2, 0, 3);
                    
       cton.setOnAction((r)->{ 
            String t = cton.getSelectionModel().getSelectedItem();
            datosPersona[4]=t;;
       }); 
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm2,cton);
       contDatos.add(mov, 0, 4);
       

   }
   
   private void fechaMigracion(){
       Label lm = new Label("Fecha de Registro:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       HBox mov = new HBox();
       date = new DatePicker();
       date.setPromptText("Fecha de Registro");
       date.setMaxWidth(300);
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm,date);
       contDatos.add(mov, 0, 5);
       fechar();
   }
   
   private void  fechar(){
       String[] s = LocalDateTime.now().toString().split("T");
       String[] feche = s[0].split("-"); 
     datosPersona[5]=feche[0];
     datosPersona[6]=feche[1];
     datosPersona[7]=feche[2];
   }
   
   private void sexoMigrante(){
       Label lm = new Label("Genero:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       HBox mov = new HBox();
        ArrayList<String> ar = new ArrayList<>();
        ar.add("Masculino");
        ar.add("Femenino");
       ComboBox<String> cblocal = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cblocal.setItems(lista);
                    cblocal.setOnAction((e)->{ 
                        String tn = cblocal.getSelectionModel().getSelectedItem();
                        datosPersona[8]=tn;
                    });
                    root.getChildren().add(cblocal);
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm,cblocal);
       contDatos.add(mov, 0, 5);
       
   }
   
   
   private void anioNacimientoMigrante(){
       Label lm = new Label("Año de Nacimiento:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       HBox mov = new HBox();
        ArrayList<String> ar = new ArrayList<>();
        ar.add("1901");ar.add("1902");ar.add("1903");ar.add("1904");ar.add("1905");
        ar.add("1906");ar.add("1907");ar.add("1908");ar.add("1909");ar.add("1910");
        ar.add("1911");ar.add("1912");ar.add("1913");ar.add("1914");ar.add("1915");
        ar.add("1916");ar.add("1917");ar.add("1918");ar.add("1919");ar.add("1920");
        ar.add("1921");ar.add("1922");ar.add("1923");ar.add("1924");ar.add("1925");
        ar.add("1926");ar.add("1927");ar.add("1928");ar.add("1929");ar.add("1930");
        ar.add("1931");ar.add("1932");ar.add("1933");ar.add("1934");ar.add("1935");
        ar.add("1936");ar.add("1937");ar.add("1938");ar.add("1939");ar.add("1940");
        ar.add("1941");ar.add("1942");ar.add("1943");ar.add("1944");ar.add("1945");
        ar.add("1946");ar.add("1947");ar.add("1948");ar.add("1949");ar.add("1950");
        ar.add("1951");ar.add("1952");ar.add("1953");ar.add("1954");ar.add("1955");
        ar.add("1956");ar.add("1957");ar.add("1958");ar.add("1959");ar.add("1960");
        ar.add("1961");ar.add("1962");ar.add("1963");ar.add("1964");ar.add("1965");
        ar.add("1966");ar.add("1967");ar.add("1968");ar.add("1969");ar.add("1970");
        ar.add("1971");ar.add("1972");ar.add("1973");ar.add("1974");ar.add("1975");
        ar.add("1976");ar.add("1977");ar.add("1978");ar.add("1979");ar.add("1980");
        ar.add("1981");ar.add("1982");ar.add("1983");ar.add("1984");ar.add("1985");
        ar.add("1986");ar.add("1987");ar.add("1988");ar.add("1989");ar.add("1990");
        ar.add("1991");ar.add("1992");ar.add("1993");ar.add("1994");ar.add("1995");
        ar.add("1996");ar.add("1997");ar.add("1998");ar.add("1999");ar.add("2000");
        ar.add("2001");ar.add("2002");ar.add("2003");ar.add("2004");ar.add("2005");
        ar.add("2006");ar.add("2007");ar.add("2008");ar.add("2009");ar.add("2010");
        ar.add("2011");ar.add("2012");ar.add("2013");ar.add("2014");ar.add("2015");ar.add("2016");
        
       ComboBox<String> cblocal = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cblocal.setItems(lista);
                    cblocal.setOnAction((e)->{ 
                        String tn = cblocal.getSelectionModel().getSelectedItem();
                        datosPersona[9]=tn;
                    });
                    
     
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm,cblocal);
       contDatos.add(mov, 0, 6);
       
   } 

   private void ocupacionMigrante(){
       Label lm = new Label("Ingrese su ocupación:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       HBox b = new HBox();
       tx.selectForward();
       UnaryOperator<Change> textLimitFilter = change -> {
            if (change.isContentChange()) {
                int newLength = change.getControlNewText().length();
                if (newLength > 30) {
                    String trimmedText = change.getControlNewText().substring(0, 30);
                    change.setText(trimmedText);
                    int oldLength = change.getControlText().length();
                    change.setRange(0, oldLength);
                }
            }
            return change;
        };
       
       //datosPersona[10]=texto;
       tx.setTextFormatter(new TextFormatter(textLimitFilter)); 
       b.setPadding(new Insets(15, 15, 15, 15));
       b.getChildren().addAll(lm,tx);
       contDatos.add(b, 1, 0);
       

   } 
   
  
   
   
   private void claseMigratoria(){
       Label lm = new Label("Elija clase migratoria:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       HBox mov = new HBox();
        ArrayList<String> ar = new ArrayList<>();
        ar.add("Clase migratoria para Ecuatorianos");
       ComboBox<String> cblocal = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cblocal.setItems(lista);
                    cblocal.setOnAction((e)->{ 
                        String tn = cblocal.getSelectionModel().getSelectedItem();
                        datosPersona[11]=tn;
                    });
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm,cblocal);
       contDatos.add(mov, 1, 1);
       
       

   } 
    
   private void motivoViaje(){
       Label lm = new Label("Elija motivo de viaje:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       HBox mov = new HBox();
        ArrayList<String> ar = new ArrayList<>();
        ar.add("Turismo");
        ar.add("Negocios");
        ar.add("Estudios");
        ar.add("Eventos");
        ar.add("Recidencia");
        ar.add("Otros");
       ComboBox<String> cblocal = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cblocal.setItems(lista);
                    cblocal.setOnAction((e)->{ 
                        String tn = cblocal.getSelectionModel().getSelectedItem();
                        datosPersona[12]=tn;
                    });
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm,cblocal);
       contDatos.add(mov, 1, 2);
       

   }
   
   private void contPaiNacionaliad(){
       Label lm = new Label("Continente de nacimiento:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       Label lm2 = new Label("País de nacimiento:  ");
       lm2.setFont(new Font("Berlin Sans FB", 15));
       Label lm3 = new Label("Subcontinente de Nacionalidad:  ");
       lm3.setFont(new Font("Berlin Sans FB", 15));
       HBox mov = new HBox();
       HBox mov2 = new HBox();
       HBox mov3 = new HBox();
       ArrayList<String> ar = ValoresRegistro.Continentes();
       ComboBox<String> cbcont = new ComboBox<>();
       ComboBox<String> cbpais = new ComboBox<>();
       ComboBox<String> cbsubcont = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cbcont.setItems(lista);
                    cbcont.setOnAction((e)->{ 
                        String tn = cbcont.getSelectionModel().getSelectedItem();
                        ArrayList<String> r2 = ValoresRegistro.asignarPais(tn);
                        ObservableList<String> listap = FXCollections.observableArrayList(r2);
                        ArrayList<String> r3 = ValoresRegistro.asignarsubContienete(tn);
                        ObservableList<String> listasub = FXCollections.observableArrayList(r3);
                        cbpais.setItems(listap);
                        cbsubcont.setItems(listasub);
                        datosPersona[20]=tn;
                    });
       mov2.setPadding(new Insets(15, 15, 15, 15));
                    mov2.getChildren().addAll(lm,cbcont);
                    contDatos.add(mov2, 1, 3);
                    
       cbpais.setOnAction((r)->{ 
            String m = cbpais.getSelectionModel().getSelectedItem();
            datosPersona[13]=m;
       }); 
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm2,cbpais);
       contDatos.add(mov, 1, 4);
        
       cbsubcont.setOnAction((l)->{ 
            String t = cbsubcont.getSelectionModel().getSelectedItem();
            datosPersona[22]=t;
       }); 
       mov3.setPadding(new Insets(15, 15, 15, 15));
       mov3.getChildren().addAll(lm3,cbsubcont);
       contDatos.add(mov3, 1, 5);

   } 
   private void contPaProcedencia(){
       Label lm = new Label("Continente de Procedencia:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       Label lm2 = new Label("País de Procedencia:  ");
       lm2.setFont(new Font("Berlin Sans FB", 15));
       Label lm3 = new Label("Subcontinente de Procedencia:  ");
       lm3.setFont(new Font("Berlin Sans FB", 15));
       HBox mov = new HBox();
       HBox mov2 = new HBox();
       HBox mov3 = new HBox();
       ArrayList<String> ar = ValoresRegistro.Continentes();
       ComboBox<String> cbcont = new ComboBox<>();
       ComboBox<String> cbpais = new ComboBox<>();
       ComboBox<String> cbsubcont = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cbcont.setItems(lista);
                    cbcont.setOnAction((e)->{ 
                        String tn = cbcont.getSelectionModel().getSelectedItem();
                        ArrayList<String> r2 = ValoresRegistro.asignarPais(tn);
                        ObservableList<String> listap = FXCollections.observableArrayList(r2);
                        ArrayList<String> r3 = ValoresRegistro.asignarsubContienete(tn);
                        ObservableList<String> listasub = FXCollections.observableArrayList(r3);
                        cbsubcont.setItems(listasub);
                        cbpais.setItems(listap);
                        datosPersona[18]=tn;
                    });
       mov2.setPadding(new Insets(15, 15, 15, 15));
                    mov2.getChildren().addAll(lm,cbcont);
                    contDatos.add(mov2, 1, 6);
                    
       cbpais.setOnAction((r)->{ 
            String t = cbpais.getSelectionModel().getSelectedItem();
            datosPersona[14]=t;
       }); 
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm2,cbpais);
       contDatos.add(mov, 1, 7);
       
        cbsubcont.setOnAction((l)->{ 
            String t = cbsubcont.getSelectionModel().getSelectedItem();
            datosPersona[21]=t;
       }); 
       mov3.setPadding(new Insets(15, 15, 15, 15));
       mov3.getChildren().addAll(lm3,cbsubcont);
       contDatos.add(mov3, 2, 0);

   } 
   private void lugarProcedencia(){
       Label lm = new Label("Elija lugar de Pocedencia:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       HBox mov = new HBox(); 
       ArrayList<String> ar = new ArrayList<>();
        ar.add("Sin Especificar");
       ComboBox<String> cblocal = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cblocal.setItems(lista);
                    cblocal.setOnAction((e)->{ 
                        String tn = cblocal.getSelectionModel().getSelectedItem();
                        datosPersona[16]=tn;
                    });
                    root.getChildren().add(cblocal);
                    mov.setPadding(new Insets(15, 15, 15, 15));
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm,cblocal);
       contDatos.add(mov, 2, 1);

   } 
   private void contPaRecidencia(){
       Label lm = new Label("Continente de Residencia:  ");
       lm.setFont(new Font("Berlin Sans FB", 15));
       Label lm2 = new Label("País de Redidencia:  ");
       lm2.setFont(new Font("Berlin Sans FB", 15));
       HBox mov = new HBox();
       HBox mov2 = new HBox();
       ArrayList<String> ar = ValoresRegistro.Continentes();
       ComboBox<String> cbcont = new ComboBox<>();
       ComboBox<String> cbpais = new ComboBox<>();
        ObservableList<String> lista = FXCollections.observableArrayList(ar);
                    cbcont.setItems(lista);
                    cbcont.setOnAction((e)->{ 
                        String tn = cbcont.getSelectionModel().getSelectedItem();
                        ArrayList<String> r2 = ValoresRegistro.asignarPais(tn);
                        ObservableList<String> listap = FXCollections.observableArrayList(r2);
                        cbpais.setItems(listap);
                        datosPersona[19]=tn;
                    });
       mov2.setPadding(new Insets(15, 15, 15, 15));
                    mov2.getChildren().addAll(lm,cbcont);
                    contDatos.add(mov2, 2, 2);
                    
       cbpais.setOnAction((r)->{ 
            String t = cbpais.getSelectionModel().getSelectedItem();
            datosPersona[15]=t;
       }); 
       mov.setPadding(new Insets(15, 15, 15, 15));
       mov.getChildren().addAll(lm2,cbpais);
       contDatos.add(mov, 2, 3);
       
   }

    public ListaRegistrosMigratorios getListado() {
        return listado;
    }

    public void setListado(ListaRegistrosMigratorios listado) {
        this.listado = listado;
    }
   
   
   
}
