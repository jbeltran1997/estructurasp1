/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

/**
 *
 * @author johnny
 */
public class TurnoEspecialGenerator implements TurnoGenerator{
    public static final char type = 'S';
    private static int count = 0;

    @Override
    public Turno generar() {
        return new Turno(type, ++count);
    }
    
    @Override
    public String toString() {
        return "Capacidad especial";
    }
}
