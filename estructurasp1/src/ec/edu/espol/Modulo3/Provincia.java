/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.Modulo3;

import java.util.Comparator;

/**
 *
 * @author Mi compu
 */
public class Provincia implements Comparable<Provincia>, Comparator<Provincia> {
    String nombre;
    int cantidad;

    public Provincia(String nombre) {
        this.nombre = nombre;
        this.cantidad = 0;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    public void aumentar(){
        this.cantidad +=1;
    }

   

   
    public boolean equals(Provincia obj) {
        if (this.cantidad == obj.cantidad) {
            return true;
        }
        return false;
    }

    
    
    @Override
    public int compareTo(Provincia other) {
        
        Provincia t = (Provincia) other;
       
       if(this.cantidad < t.cantidad){
           return 1;
       }
       else if (this.cantidad > t.cantidad){
           return -1;
       }
        return 0;   
        
    }

    @Override
    public String toString() {
        return nombre + " cantidad=" + cantidad;
    }

    @Override
    public int compare(Provincia t, Provincia t1) {
        
        
       if(t.cantidad < t1.cantidad){
           return 1;
       }
       else if(t.cantidad > t1.cantidad){
           return -1;
       }
       return 0;
    }
    
    public static Comparator<Provincia> comparator(){
        Comparator <Provincia> c = new Comparator<Provincia>() {
            @Override
            public int compare(Provincia t, Provincia t1) {                
                return Integer.valueOf(t1.cantidad).compareTo(t.cantidad);
            }
        };
                
            
      return c;      
    }
    
    
    
}
