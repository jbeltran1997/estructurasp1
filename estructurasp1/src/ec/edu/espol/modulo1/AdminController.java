/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TextField;

/**
 *
 * @author johnny
 */
public class AdminController {
    private AdminModel model;
    private AdminView view;
    private RegEncargadoView regEView;
    private RegPuestoView regPView;
    private ModifyEncargadoView modEView;
    private ModifyPuestoView modPView;
    private ObservableList<Encargado> listEncargados;
    private ObservableList<Puesto> listPuestos;
    public AdminController(AdminModel model, RegEncargadoView regEView, 
            RegPuestoView regPView, ModifyEncargadoView modEView, 
            ModifyPuestoView modPView, AdminView view) {
        this.model = model;
        this.view = view;
        this.regEView = regEView;
        this.regPView = regPView;
        this.modEView = modEView;
        this.modPView = modPView;
        listEncargados = FXCollections.
                observableArrayList(this.model.getEncargados());
        listPuestos = FXCollections.
                observableArrayList(this.model.getPuestos());
        this.modEView.getEncargadoBox().setItems(listEncargados);
        this.modPView.getPuestosBox().setItems(listPuestos);
        setChangeRootListeners();
        setListeners();
    }
    
    
    /*
        Crea un puesto con los datos ingresados y lo agrega a la lista del 
        model
    */
    public void registrarPuesto(){
        String name = this.regPView.getNombre().getText();
        String cedula = this.regPView.getNumero().getText();
        Puesto p = new Puesto(new Encargado(name, cedula));
        model.registrarEncargado(p.getEncargado());
        model.registrarPuesto(p);
        listPuestos.add(p);
    }
    /*
        Modifica un puesto con los nuevos datos
    */
    public void modificarPuesto(){
        
    }
    /*
        Elimina el puesto que se selecciona
    */
    public void eliminarPuesto(){
        Puesto p = this.modPView.getPuestosBox().getValue();
        model.eliminarPuesto(p);
        this.modPView.getPuestosBox().setValue(null);
        listPuestos.remove(p);
    }
    /*
        Agrega un encargado
    */
    public void addEncargado(){
        String name = this.regEView.getNombre().getText();
        String cedula = this.regEView.getCedula().getText();
        Encargado e = new Encargado(name, cedula);
        model.registrarEncargado(e);
        this.listEncargados.add(e);
        clearField(this.regEView.getCedula());
        clearField(this.regEView.getNombre());
    }
    /*
        Modifica un encargado
    */
    public void modifyEncargado(){
        //Encargado e = this.modEView.getEncargadoBox().getValue();
        
    }
    /*
        Elimina un encargado(){
    */
    public void deleteEncargado(){
        Encargado e = this.modEView.getEncargadoBox().getValue();
        model.eliminarEncargado(e);
        this.modEView.getEncargadoBox().setValue(null);
        listEncargados.remove(e);
    }
    
    private final void setListeners(){
        this.regEView.getRegButton().setOnAction(e->addEncargado());
        this.modEView.getModButton().setOnAction(e->{
            ModDetailsEncargado mod = new ModDetailsEncargado();
            Encargado en = model.getEncargados().get(model.getEncargados().
                    indexOf(modEView.getEncargadoBox().getValue()));
            mod.getCedula().setText(en.getCedula());
            mod.getName().setText(en.getNombreCompleto());
            this.modEView.getRoot().getScene().setRoot(mod.getRoot());
            mod.getAtras().setOnAction(g->{
                mod.getRoot().getScene().setRoot(modEView.getRoot());
            });
            
            mod.getModificar().setOnAction(f->{
                en.setCedula(mod.getCedula().getText());
                en.setNombreCompleto(mod.getName().getText());
                mod.getCedula().setText("");
                mod.getName().setText("");
                
            });
            
            //modifyEncargado();
            
        });
        this.modEView.getDelButton().setOnAction(e->deleteEncargado());
        this.regPView.getRegButton().setOnAction(e->registrarPuesto());
        this.modPView.getModButton().setOnAction(e->{
            ModDetailsPuesto mod = new ModDetailsPuesto();
            Puesto p = model.getPuestos().get(model.getPuestos().
                    indexOf(modPView.getPuestosBox().getValue()));
            mod.getCedula().setText(p.getEncargado().getCedula());
            mod.getName().setText(p.getEncargado().getNombreCompleto());
            mod.getNum().setText(String.valueOf(p.getNum()));
            this.modPView.getRoot().getScene().setRoot(mod.getRoot());
            mod.getAtras().setOnAction(g->{
                mod.getRoot().getScene().setRoot(modPView.getRoot());
            });
            
            mod.getModificar().setOnAction(f->{
                p.getEncargado().setCedula(mod.getCedula().getText());
                p.getEncargado().setNombreCompleto(mod.getName().getText());
                p.setNum(Integer.parseInt(mod.getNum().getText()));
                mod.getCedula().setText("");
                mod.getName().setText("");
                
            });
        });
        this.modPView.getDelButton().setOnAction(e->eliminarPuesto());
        
        
    }
    
    private final void setChangeRootListeners(){
        
        this.view.getAddEncargadoButton().setOnAction(e->{
            Scene current = this.view.getRoot().getScene();
            current.setRoot(regEView.getRoot());
        });
        
        this.view.getModifyEncargadoButton().setOnAction(e->{
            Scene current = this.view.getRoot().getScene();
            current.setRoot(modEView.getRoot());
        });
        
        this.view.getDeleteEncargadoButton().setOnAction(e->{
            Scene current = this.view.getRoot().getScene();
            current.setRoot(modEView.getRoot());
        });
        
        this.view.getAddPuestoButton().setOnAction(e->{
            Scene current = this.view.getRoot().getScene();
            current.setRoot(regPView.getRoot());
        });
        
        this.view.getModifyPuestoButton().setOnAction(e->{
            Scene current = this.view.getRoot().getScene();
            current.setRoot(modPView.getRoot());
        });
        
        this.view.getDeletePuestoButton().setOnAction(e->{
            Scene current = this.view.getRoot().getScene();
            current.setRoot(modPView.getRoot());
        });
        
        this.modEView.getBackButton().setOnAction(e->{
            Scene current = this.modEView.getRoot().getScene();
            current.setRoot(this.view.getRoot());
        });
        
        this.modPView.getBackButton().setOnAction(e->{
            Scene current = this.modPView.getRoot().getScene();
            current.setRoot(this.view.getRoot());
        });
        
        this.regEView.getBackButton().setOnAction(e->{
            Scene current = this.regEView.getRoot().getScene();
            current.setRoot(this.view.getRoot());
        });
        
        this.regPView.getBackButton().setOnAction(e->{
            Scene current = this.regPView.getRoot().getScene();
            current.setRoot(this.view.getRoot());
        });
    }
    
    private static void clearField(TextField field){
        field.setText("");
    }
}
