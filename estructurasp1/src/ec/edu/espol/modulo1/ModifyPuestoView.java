/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author johnny
 */
public class ModifyPuestoView {
    private ComboBox<Puesto> puestosBox;
    private Button modificarButton;
    private Button eliminarButton;
    private Pane root;
    private Button backButton;
    public ModifyPuestoView(){
        puestosBox = new ComboBox<>();
        modificarButton = new Button("Modificar");
        eliminarButton = new Button("Eliminar");
        backButton = new Button("Atrás");
        root = new VBox(puestosBox, modificarButton, eliminarButton, backButton);
    }
    public Button  getModButton() {
        return modificarButton;
    }

    public Button  getDelButton() {
        return eliminarButton;
    }
    
    public Pane getRoot() {
        return root;
    }

    public ComboBox<Puesto> getPuestosBox() {
        return puestosBox;
    }

    public Button getBackButton() {
        return backButton;
    }
    
    
}
