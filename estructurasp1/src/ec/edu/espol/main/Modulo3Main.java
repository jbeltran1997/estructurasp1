/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.main;

import java.io.IOException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author johnny
 */
public class Modulo3Main extends Application {
    public static Scene scene;
    PaneOrganizerModulo3 pn;
    @Override
    public void start(Stage primaryStage) throws IOException {
        scene = new Scene(new Group(),1200,670);
        pn= new PaneOrganizerModulo3();
        scene.setRoot(pn.getRoot());
        primaryStage.setScene(scene);
        primaryStage.setTitle("Modulo3");
        
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
