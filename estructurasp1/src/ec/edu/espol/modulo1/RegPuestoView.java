/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author johnny
 */
public class RegPuestoView {
    private TextField nombre;
    private TextField numero;
    private Button registrarButton;
    private Pane root;
    private Button backButton;
    public RegPuestoView() {
        nombre = new TextField("Nombre completo");
        numero = new TextField("Cédula de identidad");
        registrarButton = new Button("Registrar");
        backButton = new Button("Atrás");
        root = new VBox();
        ((VBox)root).setAlignment(Pos.CENTER);
        root.getChildren().addAll(nombre, numero, registrarButton, backButton);
    }
    public Button getRegButton() {
        return registrarButton;
    }

    public Pane getRoot() {
        return root;
    }

    public TextField getNombre() {
        return nombre;
    }

    public TextField getNumero() {
        return numero;
    }

    public Button getBackButton() {
        return backButton;
    }
    
    
}
