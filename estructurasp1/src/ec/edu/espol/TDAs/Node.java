/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.TDAs;

/**
 *
 * @author johnny
 */
public class Node<E> {
    private E data;
    private Node next;

    public Node(E data) {
        this.data = data;
    }
    
    
    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public Node<E> getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
    
    
}
