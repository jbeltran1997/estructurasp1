/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import ec.edu.espol.TDAs.CircularSimplyLinkedList;
import static ec.edu.espol.modulo1.PrincipalController.VIDEOS_FILE;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

/**
 *
 * @author johnny
 */
public class PrincipalView {
    public static final int MAX_TURNOS = 5;
    public static final String DEFAULT_MEDIA_SOURCE = "default.mp4";
    public static final String HEADER_PUESTO = "Puesto";
    public static final String HEADER_TURNO = "Turno";
    private MediaPlayer mediaPlayer;
    private Media media;
    private MediaView mediaView;
    private VBox turnosContainer;
    private Pane root;
    private Label[] turnos;
    private List<String> videos;
    private CircularSimplyLinkedList<MediaPlayer> mediaPlayers;
    
    public PrincipalView(){
        videos = new ArrayList<>();
        mediaPlayers = new CircularSimplyLinkedList<>();
        root = new HBox();
        media = new Media(new File(DEFAULT_MEDIA_SOURCE).toURI().toString());
        mediaView = new MediaView();
        turnosContainer = new VBox();
        turnos = new Label[MAX_TURNOS];
        for (int i = 0; i < MAX_TURNOS; i++) {
            turnos[i] = new Label();
        }
        organize();
        try{
            processVideos();
            for(int i = 0; i < videos.size(); i++){
                mediaPlayers.addLast(new MediaPlayer(new Media(videos.get(i))));
            }
            
        }catch(IOException e){
            System.out.println("NO EXISTE EL ARCHIVO");
        }
        Iterator<MediaPlayer> it = mediaPlayers.iterator();
        
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if(it.hasNext()){
                    MediaPlayer player = it.next();
                    player.stop();
                    System.out.println(player.getStatus());
                    player.play();
                    player.setOnEndOfMedia(this);
                    mediaView.setMediaPlayer(player);
                    System.out.println("A loop");
                }
            }
        });
        t.start();
    }
    
    public Media getMedia(){
        return media;
    }
    
    public void setMedia(Media media){
        this.media = media;
    }
    
    public void organize(){
        root.getChildren().add(mediaView);
        root.getChildren().add(turnosContainer);
        turnos[0].setText(String.format("%s%20s", HEADER_PUESTO, HEADER_TURNO));
        for(int i = 0; i < MAX_TURNOS; i++){
            turnosContainer.getChildren().add(turnos[i]);
        }
        ((HBox)root).setAlignment(Pos.CENTER_LEFT);
    }
    
    public Label[] getTurnos(){
        return turnos;
    }
    
    public Pane getRoot(){
        return root;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public MediaView getMediaView() {
        return mediaView;
    }

    public void setMediaView(MediaView mediaView) {
        this.mediaView = mediaView;
    }
    
    private void processVideos() throws IOException{
        BufferedReader bf = new BufferedReader(new FileReader(VIDEOS_FILE));
        String line;
        int i = 0;
        while((line = bf.readLine())!=null){
            videos.add(new File(line).toURI().toString());
        }
        bf.close();
    }
    private void loopVideos(MediaView mediaView, Iterator<MediaPlayer> iterator){
        if(iterator.hasNext()){
            MediaPlayer player = iterator.next();
            player.setAutoPlay(true);
            player.setOnEndOfMedia(new Runnable() {
                @Override
                public void run() {
                    
                    loopVideos(mediaView, iterator);
                }
            });
            mediaView.setMediaPlayer(player);
            System.out.println("finished a loop");
            System.out.println(player.getMedia().toString());
        }
    }
    private void loopVideos2(MediaView mediaView, Iterator<MediaPlayer> iterator){
        if(iterator.hasNext()){
            MediaPlayer player = iterator.next();
            player.setAutoPlay(true);
            player.setOnEndOfMedia(new Runnable() {
                @Override
                public void run() {
                    MediaPlayer player2 = iterator.next();
                    player2.setAutoPlay(true);
                    mediaView.setMediaPlayer(player2);
                }
            });
            mediaView.setMediaPlayer(player);
            System.out.println("finished a loop");
            System.out.println(player.getMedia().toString());
        }
    }
}
