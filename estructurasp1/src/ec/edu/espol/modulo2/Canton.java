/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo2;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import javafx.beans.property.SimpleStringProperty;


/**
 *
 * @author Nicole Alvarez
 */
public class Canton {
    private final String filename = "src/archivos/Canton.txt";
    private String nombre_canton;
    protected static int ID_pro=0;
    private String id;
    private LinkedList<RegistroMigratorio> registros;
    

    public Canton  (String nombre_canton,String id) {
        this.nombre_canton = nombre_canton;
        this.id=id;
        registros = new LinkedList<>();
        
    }
    
    public Canton  (String nombre_canton) {
        this.nombre_canton = nombre_canton;
        this.id=null;
    }
    
    
   
   public void escribirCanton(){
       try(BufferedWriter outputStream =
                 new BufferedWriter(new FileWriter(filename,true)))
        {
                
                outputStream.write(nombre_canton+","+id);
                outputStream.newLine();
                outputStream.close();
            
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error opening the file out.txt."+ e.getMessage());
        }
        catch(IOException e){
            System.out.println("IOException."+ e.getMessage());
        }
   
   }

    public String getNombre_canton() {
        return nombre_canton;
    }

    public void setNombre_canton(String nombre_canton) {
        this.nombre_canton = nombre_canton;
    }

    

    public LinkedList<RegistroMigratorio> getRegistros() {
        return registros;
    }

    public void setRegistros(LinkedList<RegistroMigratorio> registros) {
        this.registros = registros;
    }
   
    
    public LinkedList<RegistroMigratorio> getRegistro() {
        return registros;
    }

    public void setRegistro(LinkedList<RegistroMigratorio> registros) {
        this.registros = registros;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return nombre_canton;
    }

    
    
    
}
