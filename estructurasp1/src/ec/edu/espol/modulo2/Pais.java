/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo2;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import javafx.beans.property.SimpleStringProperty;


/**
 *
 * @author Nicole Alvarez
 */
public class Pais{
    private final String filename = "src/archivos/Pais.txt";
    //private final String fileName="src/archivos/migraciones.csv";
    private String nombre_pais,id;
    private LinkedList<Provincia> provincias;
     private LinkedList<RegistroMigratorio> registros;

    public Pais(String nombre_pais,String id) {
        this.nombre_pais = nombre_pais;
        this.id= id;
        provincias = new LinkedList<>();
        registros = new LinkedList<>();
        
    }
    
    public Pais(String nombre_pais) {
        this.nombre_pais = nombre_pais;
        this.id=null;
    }
    
   public void agregarProvincia(Provincia p){
       if(p!=null&&!provincias.contains(p)){
           provincias.add(p);
       } 
   }
   
   public void escribirPais(){
       try(BufferedWriter outputStream =
                 new BufferedWriter(new FileWriter(filename,true)))
        {
                
                outputStream.write(nombre_pais+","+id);
                outputStream.newLine();
                outputStream.close();
            
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error opening the file out.txt."+ e.getMessage());
        }
        catch(IOException e){
            System.out.println("IOException."+ e.getMessage());
        }
   
   }

    public String getNombre_pais() {
        return nombre_pais;
    }

    public void setNombre_pais(String nombre_pais) {
        this.nombre_pais = nombre_pais;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    
   
   
    public LinkedList<Provincia> getProvincias() {
        return provincias;
    }

    public void setProvincias(LinkedList<Provincia> provincias) {
        this.provincias = provincias;
    }

    public LinkedList<RegistroMigratorio> getRegistros() {
        return registros;
    }

    public void setRegistros(LinkedList<RegistroMigratorio> registros) {
        this.registros = registros;
    }

    

    @Override
    public String toString() {
        return nombre_pais;
    }
    
    
    
    
}
