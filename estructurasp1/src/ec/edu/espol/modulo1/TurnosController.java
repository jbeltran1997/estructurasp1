/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.scene.control.ComboBox;

/**
 *
 * @author johnny
 */
public class TurnosController {
    private TurnosView view;
    private AtencionTurnosModel model;
    public TurnosController(AtencionTurnosModel model, TurnosView view){
        this.view = view;
        this.model = model;
        this.view.getGenerarButton().setOnAction(e->{
            ComboBox<TurnoGenerator> tipos = this.view.getTipoComboBox();
            TurnoGenerator generator = tipos.getValue();
            Turno turno = generator.generar();
            this.model.encolar(turno);
        });
    }
    
    
}
