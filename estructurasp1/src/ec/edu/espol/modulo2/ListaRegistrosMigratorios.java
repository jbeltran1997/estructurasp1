/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;


/**
 *
 * @author Nicole Alvarez
 */
public class ListaRegistrosMigratorios {
    LinkedList<RegistroMigratorio> registros;
    private String filename = "src/archivos/RegistroMigratorio.txt";
    
    public ListaRegistrosMigratorios(){
        registros = new LinkedList<>();
        LeerRegistro();
        
     }
    
    
    private void LeerRegistro(){ 
        try(BufferedReader inputStream = new BufferedReader(new FileReader(filename));)
        { 
            String line = null; 
            
            //RegistroMigratorio  r; 
            while( (line = inputStream.readLine()) != null ){
                String s[] = line.split(","); 
                String id = s[0];
                Pais ppro = new Pais(s[15],id);
                Pais pres = new Pais(s[16],id);
                Pais pa_registro = new Pais(s[14],id);
                Continente con_pro = new Continente(s[18],id);
                Continente con_re = new Continente(s[19],id);
                Continente con_na = new Continente(s[20],id);
                Continente subcon_pro = new Continente(s[21],id);
                Continente subcon_na = new Continente(s[22],id);
                Provincia pro_registro = new Provincia (s[4],id);
                Canton can_registro = new Canton(s[5],id);
                Migrante migrante = new Migrante(id, s[2], s[9], s[10], s[11], ppro, pres, con_pro, con_re, con_na, subcon_pro, subcon_na);
                RegistroMigratorio reg =new RegistroMigratorio(id,s[1],s[2], s[6],s[7], s[8], s[12], s[13], migrante, pro_registro, can_registro, pa_registro);
                registros.add(reg);
            }
        }
        catch(FileNotFoundException e)
        {
          System.out.println("File " + filename + " not found.");
        }
        catch(IOException e)
        {
           System.out.println("Error reading from file " + filename);
        }
    
    }
    
    
    public LinkedList<RegistroMigratorio> getRegistros(){
        return registros;
    }
    
    public void eliminarRegistroMigratorio(RegistroMigratorio rm) {
        System.out.println(registros.size());
        registros.remove(rm);
        System.out.println(registros.size());
        //try
        //{
        
            //Files.delete(FileSystems.getDefault().getPath("src/archivos/", "RegistroMigratorio.txt"));
             File fichero = new File("src/archivos/RegistroMigratorio.txt");

            if (fichero.delete())
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            else
                System.out.println("El fichero no pudó ser borrado");
        
        //}catch(IOException e){         
        //}
        
        //rm.EliminarRegistro();
        actualizarRegistro();
        
        
    }
    
    private void actualizarRegistro(){
        for(RegistroMigratorio r:registros){
            r.GuardarRegistro();
        }
    }
    
    
    public void anadirRegistro(RegistroMigratorio reg) throws Exception{
       if(reg.getCant_regmigra().getNombre_canton()==null || reg.getClase_migra()==null
                || reg.getMigrante().getGenero()==null 
               || reg.getMigrante().getAnio_nacimiento()==null || reg.getMigrante().getCont_pro().getNombre_continente()==null 
               || reg.getMigrante().getCont_res().getNombre_continente()==null || reg.getMigrante().getContinen_nac().getNombre_continente()==null
               || reg.getMigrante().getLugarpro().getNombre_pais()==null || reg.getMigrante().getLugarres().getNombre_pais()==null 
               || reg.getMotivo_viaje()==null
               || reg.getProvincia().getNombre_provincia()==null || reg.getTipo_migracion()==null && reg.getVia_transporte()==null
               || reg.getTipo_migracion()==null
               ){
           throw new Exception();   
       }     
       
       registros.add(reg);
            reg.getCant_regmigra().escribirCanton();
            reg.getMigrante().getCont_pro().escribirContinente();
            reg.getMigrante().getCont_res().escribirContinente();
            reg.getMigrante().getContinen_nac().escribirContinente();
            reg.getMigrante().getSubcont_pro().escribirContinente();
            reg.getMigrante().getSuncont_res().escribirContinente();
            reg.getMigrante().getLugarpro().escribirPais();
            reg.getMigrante().getLugarres().escribirPais();
            reg.getMigrante().escribirMigrante();
            reg.getProvincia().escribirProvincia();
            reg.GuardarRegistro();
        
    }
    
}