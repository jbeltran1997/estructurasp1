/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.Modulo3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Stack;

/**
 *
 * @author Mi compu
 */
public class Modulo3 {
    
    String linea;
    FileReader fr;
    ArrayList <Provincia>  lista = new ArrayList(24);
    ArrayList<Provincia> lista_salida = new ArrayList(24);

    public Modulo3() throws FileNotFoundException, IOException {
        
        
        lista.add(new Provincia("Guayas"));
        lista.add(new Provincia("Esmeraldas"));
        lista.add(new Provincia("Manabí"));
        lista.add(new Provincia("Los Ríos"));
        lista.add(new Provincia("El Oro"));
        lista.add(new Provincia("Santa Elena"));
        lista.add(new Provincia("Sucumbíos"));
        lista.add(new Provincia("Napo"));
        lista.add(new Provincia("Pastaza"));
        lista.add(new Provincia("Orellana"));
        lista.add(new Provincia("Morona Santiago"));
        lista.add(new Provincia("Zamora Chinchipe"));
        lista.add(new Provincia("Carchi"));
        lista.add(new Provincia("Imbabura"));
        lista.add(new Provincia("Pichincha"));
        lista.add(new Provincia("Cotopaxi"));
        lista.add(new Provincia("Tungurahua"));
        lista.add(new Provincia("Bolívar"));
        lista.add(new Provincia("Chimborazo"));
        lista.add(new Provincia("Cañar"));
        lista.add(new Provincia("Azuay"));
        lista.add(new Provincia("Loja"));
        lista.add(new Provincia("Sto. Domingo de los Tsachilas"));
        lista.add(new Provincia("Galápagos"));
        
        //lista_salida =  (ArrayList) lista.clone();
        
        lista_salida.add(new Provincia("Guayas"));
        lista_salida.add(new Provincia("Esmeraldas"));
        lista_salida.add(new Provincia("Manabí"));
        lista_salida.add(new Provincia("Los Ríos"));
        lista_salida.add(new Provincia("El Oro"));
        lista_salida.add(new Provincia("Santa Elena"));
        lista_salida.add(new Provincia("Sucumbíos"));
        lista_salida.add(new Provincia("Napo"));
        lista_salida.add(new Provincia("Pastaza"));
        lista_salida.add(new Provincia("Orellana"));
        lista_salida.add(new Provincia("Morona Santiago"));
        lista_salida.add(new Provincia("Zamora Chinchipe"));
        lista_salida.add(new Provincia("Carchi"));
        lista_salida.add(new Provincia("Imbabura"));
        lista_salida.add(new Provincia("Pichincha"));
        lista_salida.add(new Provincia("Cotopaxi"));
        lista_salida.add(new Provincia("Tungurahua"));
        lista_salida.add(new Provincia("Bolívar"));
        lista_salida.add(new Provincia("Chimborazo"));
        lista_salida.add(new Provincia("Cañar"));
        lista_salida.add(new Provincia("Azuay"));
        lista_salida.add(new Provincia("Loja"));
        lista_salida.add(new Provincia("Sto. Domingo de los Tsachilas"));
        lista_salida.add(new Provincia("Galápagos"));
        
        this.fr = new FileReader("migraciones.csv");
        BufferedReader bf = new BufferedReader(fr);
        
        //System.out.println(bf.readLine() + "\n");
        linea= bf.readLine();
        while((linea = bf.readLine()) != null){
        
            //System.out.println(linea);
            
            String [] arreglo = linea.split(";");
            
            if(arreglo[0].endsWith("Entrada")){
                
                for(int x =0 ; x<24;x++){
                    
                    if(lista.get(x).nombre.equalsIgnoreCase(arreglo[3])){
                        lista.get(x).aumentar();
                                              
                    }
                }                
            }           
            
            else if(arreglo[0].endsWith("Salida")){                
                for(int x =0 ; x<24;x++){
                    
                    if(lista_salida.get(x).nombre.equalsIgnoreCase(arreglo[3])){
                        lista_salida.get(x).aumentar();                        
                        
                    }
                }                
            }        
        }
        
        File file = new File("src/archivos/RegistroMigratorio.txt");
        
        if(file.exists()){
            
            
        bf = new BufferedReader(new FileReader("src/archivos/RegistroMigratorio.txt"));
        
        while((linea = bf.readLine()) != null){
            
          String [] arreglo = linea.split(",");
          
          if(arreglo.length < 4){
              System.out.println("Problema de archivo");
              break;
          }
            
            if(arreglo[1].endsWith("Entrada")){
                
                for(int x =0 ; x<24;x++){
                    
                    if(lista.get(x).nombre.equalsIgnoreCase(arreglo[4])){
                        lista.get(x).aumentar();
                                              
                    }
                }                
            }           
            
            else if(arreglo[1].endsWith("Salida")){                
                for(int x =0 ; x<24;x++){
                    
                    if(lista_salida.get(x).nombre.equalsIgnoreCase(arreglo[4])){
                        lista_salida.get(x).aumentar();                        
                        
                    }
                }                
            }  
            
            
        }
        }
        else{
            System.out.println("No se encontro archivo RegistroMigratorio.txt");
        }
        
    }
    
    
    public ArrayList<Deque> pilasregion(){
        
        LinkedList <Provincia> costa_e = new LinkedList<>();
        LinkedList <Provincia> costa_s = new LinkedList<>();

        LinkedList <Provincia> sierra_e = new LinkedList<>();

        LinkedList <Provincia> sierra_s = new LinkedList<>();
        LinkedList <Provincia> oriente_e = new LinkedList<>();
        LinkedList<Provincia> oriente_s = new LinkedList<>();
        LinkedList <Provincia> insular_e = new LinkedList<>();
        LinkedList <Provincia> insular_s = new LinkedList<>();
           
        ArrayList<Integer> cantidad_e = new ArrayList();
        ArrayList<Integer> cantidad_s = new ArrayList();
        
        
        
        int n = 0;
        for(Provincia i : lista){
            cantidad_e.add(i.cantidad);
            
            if(n<6){
                costa_e.add(i);
            }
            if(n>=6 && n<12){
                oriente_e.add(i);
            }
            if(n>=12 && n<23){
                sierra_e.add(i);
            }
            if(n==23){
                insular_e.add(i);
            }

            n++;
        }
        
        n=0;
        for(Provincia i : lista_salida){
            cantidad_s.add(i.cantidad);
            
            if(n<6){
                costa_s.add(i);
            }
            if(n>=6 && n<12){
                oriente_s.add(i);
            }
            if(n>=12 && n<23){
                sierra_s.add(i);
            }
            if(n==23){
                insular_s.add(i);
            }
            n++;
            
        }
        
        Collections.sort(lista, Provincia.comparator());        
        Collections.sort(lista_salida, Provincia.comparator());
        
        Collections.sort(costa_e, Provincia.comparator());
        Collections.sort(costa_s, Provincia.comparator());
        Collections.sort(sierra_e, Provincia.comparator());
        Collections.sort(sierra_s, Provincia.comparator());
        Collections.sort(oriente_e, Provincia.comparator());
        Collections.sort(oriente_s, Provincia.comparator());
        Collections.sort(insular_e, Provincia.comparator());
        Collections.sort(insular_s, Provincia.comparator());
        
        
        System.out.println(lista);
        System.out.println(insular_e);
      
        
        
        ArrayList <Deque> pilas_region = new ArrayList();
        pilas_region.add((Deque)costa_e);
        pilas_region.add((Deque)costa_s);
        pilas_region.add((Deque)sierra_e);
        pilas_region.add((Deque)sierra_s);
        pilas_region.add((Deque)oriente_e);
        pilas_region.add((Deque)oriente_s);
        pilas_region.add((Deque)insular_e);
        pilas_region.add((Deque)insular_s);
        
        return pilas_region;
    }
    
public ArrayList<ArrayList> pilas_es(){
        
        ArrayList <Provincia>lista1 = lista;
        ArrayList <Provincia>lista2 = lista_salida;
        
        Collections.sort(lista1, Provincia.comparator());        
        Collections.sort(lista2, Provincia.comparator());
        
        ArrayList arreglo = new ArrayList();
        
        arreglo.add( lista1);
        arreglo.add( lista2);
        
        return arreglo;
    }
    
}
