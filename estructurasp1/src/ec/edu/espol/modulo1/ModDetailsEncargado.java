/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author johnny
 */
public class ModDetailsEncargado {
    private TextField name;
    private TextField cedula;
    private Button modificar;
    private Button atras;
    private Pane root;
    
    public ModDetailsEncargado(){
        name = new TextField();
        cedula = new TextField();
        modificar = new Button("Modificar");
        atras = new Button("Atras");
        root = new VBox(name, cedula, modificar, atras);
    }

    public Button getModificar() {
        return modificar;
    }

    public TextField getName() {
        return name;
    }

    public void setName(TextField name) {
        this.name = name;
    }

    public TextField getCedula() {
        return cedula;
    }

    public void setCedula(TextField cedula) {
        this.cedula = cedula;
    }

    public Pane getRoot() {
        return root;
    }

    public Button getAtras() {
        return atras;
    }
    
    
    
}
