/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.main;

import ec.edu.espol.ayudadores.StageClass;
import ec.edu.espol.modulo2.Canton;
import ec.edu.espol.modulo2.Continente;
import ec.edu.espol.modulo2.Migrante;
import ec.edu.espol.modulo2.Pais;
import ec.edu.espol.modulo2.Provincia;
import ec.edu.espol.modulo2.RegistroMigratorio;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;


/**
 *
 * @author Nicole Alvarez
 */
public class PaneOrganizerModulo21 {
    private VBox root;
    private TableView<RegistroMigratorio> table;
    private Label label;
    private Background Bg;
    private StageClass stage;
    private int cou=0;

    public VBox getRoot() {
        return root;
    }

    public PaneOrganizerModulo21() {
        root = new VBox();
        table = new TableView();
        label = new Label("Registros Migratorios");
        label.setFont(new Font("Berlin Sans FB", 20));
        table.setEditable(true);
        initColum();
        backgroundStage();
        initButton();
 
    }
    
    private void initColum(){
        TableColumn <RegistroMigratorio, String> tip_migracion = new TableColumn("Tipo de migracion");
        TableColumn <RegistroMigratorio, String> tip_nacionalidad = new TableColumn("Tipo de Nacionalidad");
        TableColumn <RegistroMigratorio, String> via_transporte = new TableColumn("Via de Transporte");
        TableColumn <RegistroMigratorio, String> pro_jefmigrante = new TableColumn("Provincia de jefatura de migrante");
        TableColumn <RegistroMigratorio, String> can_jefmigrante = new TableColumn("Canton de jefatura de migrante");
        TableColumn <RegistroMigratorio, String> ani_movi = new TableColumn("Año de movimiento");
        TableColumn <RegistroMigratorio, String> mes_movi = new TableColumn("Mes de movimiento");
        TableColumn <RegistroMigratorio, String> dia_movi = new TableColumn("Día de movimiento");
        TableColumn <RegistroMigratorio, String> sex_migr = new TableColumn("Sexo del migrante");
        TableColumn <RegistroMigratorio, String> ani_nacm = new TableColumn("Año de nacimiento");
        TableColumn <RegistroMigratorio, String> ocu_migr = new TableColumn("Ocupación del migrante");
        TableColumn <RegistroMigratorio, String> cla_migr = new TableColumn("Clase Migratoria");
        TableColumn <RegistroMigratorio, String> mot_viaje = new TableColumn("Motivo del viaje");
        TableColumn <RegistroMigratorio, String> nac_migr = new TableColumn("Nacionalidad");
        TableColumn <RegistroMigratorio, String> pais_prosedencia  = new TableColumn("Pais de procedencia");
        TableColumn <RegistroMigratorio, String> pais_residencia = new TableColumn("Pais de residencia");
        TableColumn <RegistroMigratorio, String> lug_procedencia = new TableColumn("Lugar de procedencia");
        TableColumn <RegistroMigratorio, String> edad = new TableColumn("Edad");
        TableColumn <RegistroMigratorio, String> cont_procedencia = new TableColumn("Continente de procedencia");
        TableColumn <RegistroMigratorio, String> cont_residencia = new TableColumn("Continente de residencia");
        TableColumn <RegistroMigratorio, String> cont_nacionalidad = new TableColumn("Continente de nacinalidad");
        TableColumn <RegistroMigratorio, String> subcont_prodencia = new TableColumn("Subcontinente de procedencia");
        TableColumn <RegistroMigratorio, String> subcont_nacionalidad = new TableColumn("Subcontinente de nacionalidad");
          
        tip_migracion.setCellValueFactory(new PropertyValueFactory<>("tip_migracion"));
        tip_nacionalidad.setCellValueFactory(new PropertyValueFactory<>("tip_nacionalidad"));
        via_transporte.setCellValueFactory(new PropertyValueFactory<>("via_transporte"));
        pro_jefmigrante.setCellValueFactory(new PropertyValueFactory<>("pro_jefmigrante"));
        can_jefmigrante.setCellValueFactory(new PropertyValueFactory<>("can_jefmigrante"));
        ani_movi.setCellValueFactory(new PropertyValueFactory<>("ani_movi"));
        mes_movi.setCellValueFactory(new PropertyValueFactory<>("mes_movi"));
        dia_movi.setCellValueFactory(new PropertyValueFactory<>("dia_movi"));
        sex_migr.setCellValueFactory(new PropertyValueFactory<>("sex_migr"));
        ani_nacm.setCellValueFactory(new PropertyValueFactory<>("ani_nacm"));
        ocu_migr.setCellValueFactory(new PropertyValueFactory<>("ocu_migr"));
        cla_migr.setCellValueFactory(new PropertyValueFactory<>("cla_migr"));
        mot_viaje.setCellValueFactory(new PropertyValueFactory<>("mot_viaje migrante"));
        nac_migr.setCellValueFactory(new PropertyValueFactory<>("nac_migr"));
        pais_prosedencia .setCellValueFactory(new PropertyValueFactory<>("pais_prosedencia "));
        pais_residencia.setCellValueFactory(new PropertyValueFactory<>("pais_residencia"));
        lug_procedencia.setCellValueFactory(new PropertyValueFactory<>("lug_procedencia"));
        edad.setCellValueFactory(new PropertyValueFactory<>("edad"));
        cont_procedencia.setCellValueFactory(new PropertyValueFactory<>("cont_procedencia"));
        cont_residencia.setCellValueFactory(new PropertyValueFactory<>("cont_residencia"));
        cont_nacionalidad.setCellValueFactory(new PropertyValueFactory<>("cont_nacionalidad"));
        subcont_prodencia.setCellValueFactory(new PropertyValueFactory<>("subcont_prodencia"));
        subcont_nacionalidad.setCellValueFactory(new PropertyValueFactory<>("subcont_nacionalidad"));
        
        ArrayList<String> id_ = new ArrayList<>();
        ArrayList<String> tip_migracion1 = new ArrayList<>();
        ArrayList<String> tip_nacionalidad1 = new ArrayList<>();
        ArrayList<String> via_transporte1 = new ArrayList<>();
        ArrayList<String> pro_jefmigrante1 = new ArrayList<>();
        ArrayList<String> can_jefmigrante1 = new ArrayList<>();
        ArrayList<String> ani_movi1 = new ArrayList<>();
        ArrayList<String> mes_movi1 = new ArrayList<>();
        ArrayList<String> dia_movi1 = new ArrayList<>();
        ArrayList<String> sex_migr1 = new ArrayList<>();
        ArrayList<String> ani_nacm1 = new ArrayList<>();
        ArrayList<String> ocu_migr1 = new ArrayList<>();
        ArrayList<String> cla_migr1 = new ArrayList<>();
        ArrayList<String> mot_viaje1 = new ArrayList<>();
        ArrayList<String> nac_migr1 = new ArrayList<>();
        ArrayList<String> pais_prosedencia1 = new ArrayList<>();
        ArrayList<String> pais_residencia1 = new ArrayList<>();
        ArrayList<String> lug_procedencia1 = new ArrayList<>();
        ArrayList<String> edad1 = new ArrayList<>();
        ArrayList<String> cont_procedencia1 = new ArrayList<>();
        ArrayList<String> cont_residencia1 = new ArrayList<>();
        ArrayList<String> cont_nacionalidad1 = new ArrayList<>();
        ArrayList<String> subcont_prodencia1 = new ArrayList<>();
        ArrayList<String> subcont_nacionalidad1 = new ArrayList<>();
        
         
        for(RegistroMigratorio r:PaneOrganizerModulo2.listado.getRegistros()){    
            id_.add(r.getId());
            tip_migracion1.add(r.getTipo_migracion());
            tip_nacionalidad1.add(r.getMigrante().getNacionalidad());
            via_transporte1.add(r.getVia_transporte());
            pro_jefmigrante1.add(r.getProvincia().getNombre_provincia());
            can_jefmigrante1.add(r.getCant_regmigra().getNombre_canton());
            ani_movi1.add(r.getAnio_movi());
            mes_movi1.add(r.getMes_movi());
            dia_movi1.add(r.getDia_movi());
            sex_migr1.add(r.getMigrante().getGenero());
            ani_nacm1.add(r.getMigrante().getAnio_nacimiento());
            ocu_migr1.add(r.getMigrante().getOcupacion());
            cla_migr1.add(r.getClase_migra());
            mot_viaje1.add(r.getMotivo_viaje());
            nac_migr1.add(r.getMigrante().getNacionalidad());
            pais_prosedencia1.add(r.getMigrante().getLugarpro().getNombre_pais());
            pais_residencia1.add(r.getMigrante().getLugarres().getNombre_pais());
            lug_procedencia1.add("Sin Especificar");
            edad1.add(String.valueOf(r.getMigrante().getEdad()));
            cont_procedencia1.add(r.getMigrante().getCont_pro().getNombre_continente());
            cont_residencia1.add(r.getMigrante().getCont_res().getNombre_continente());
            cont_nacionalidad1.add(r.getMigrante().getContinen_nac().getNombre_continente());
            subcont_prodencia1.add(r.getMigrante().getSubcont_pro().getNombre_continente());
            subcont_nacionalidad1.add(r.getMigrante().getSuncont_res().getNombre_continente());
        }
        
        
        for(int i=0;i<tip_migracion1.size();i++){
        funcionGeneradora(tip_migracion, tip_migracion1, i);
        funcionGeneradora(tip_nacionalidad, tip_nacionalidad1, i);
        funcionGeneradora(via_transporte, via_transporte1, i);
        funcionGeneradora(pro_jefmigrante, pro_jefmigrante1, i);
        funcionGeneradora(can_jefmigrante, can_jefmigrante1, i);
        funcionGeneradora(ani_movi, ani_movi1, i);
        funcionGeneradora(mes_movi, mes_movi1, i);
        funcionGeneradora(dia_movi , dia_movi1 , i);
        funcionGeneradora(sex_migr, sex_migr1, i);
        funcionGeneradora(ani_nacm, ani_nacm1, i);
        funcionGeneradora(ocu_migr, ocu_migr1, i);
        funcionGeneradora(cla_migr, cla_migr1, i);
        funcionGeneradora(mot_viaje , mot_viaje1 , i);
        funcionGeneradora(sex_migr, sex_migr1, i);
        funcionGeneradora(nac_migr, nac_migr1, i);
        funcionGeneradora(pais_prosedencia,pais_prosedencia1,i);
        funcionGeneradora(pais_residencia,pais_residencia1,i); 
        funcionGeneradora(lug_procedencia,lug_procedencia1,i);
        funcionGeneradora(edad,edad1 ,i);
        funcionGeneradora(cont_procedencia,cont_procedencia1 ,i);
        funcionGeneradora(cont_residencia,cont_residencia1 ,i);
        funcionGeneradora(cont_nacionalidad,cont_nacionalidad1,i);
        funcionGeneradora(subcont_prodencia,cont_nacionalidad1,i);
        funcionGeneradora(subcont_nacionalidad,subcont_nacionalidad1 ,i);
        
        
        }
        
       
       table.getColumns().addAll(tip_migracion,tip_nacionalidad,via_transporte,pro_jefmigrante,can_jefmigrante,ani_movi,mes_movi,
        dia_movi,sex_migr,ani_nacm,ocu_migr,cla_migr,mot_viaje,nac_migr,pais_prosedencia,pais_residencia,lug_procedencia,
	edad,cont_procedencia,cont_residencia,cont_nacionalidad,subcont_prodencia,subcont_nacionalidad);
        
       
            for (int i = 0; i < 1; i++) {
                String id = id_.get(i);
                Pais ppro = new Pais(pais_prosedencia1.get(i),id);
                Pais pres = new Pais(pais_residencia1.get(i),id);
                Pais pa_registro = new Pais(nac_migr1.get(i),id);
                Continente con_pro = new Continente(cont_procedencia1.get(i),id);
                Continente con_re = new Continente(cont_residencia1.get(i),id);
                Continente con_na = new Continente(cont_nacionalidad1.get(i),id);
                Continente subcon_pro = new Continente(subcont_prodencia1.get(i),id);
                Continente subcon_na = new Continente(subcont_nacionalidad1.get(i),id);
                Provincia pro_registro = new Provincia (pro_jefmigrante1.get(i),id);
                Canton can_registro = new Canton(can_jefmigrante1.get(i),id);
                Migrante migrante = new Migrante(id, nac_migr1.get(i), sex_migr1.get(i), ani_nacm1.get(i), ocu_migr1.get(i), ppro, pres, con_pro, con_re, con_na, subcon_pro, subcon_na);
                RegistroMigratorio reg =new RegistroMigratorio(id,tip_migracion1.get(i),via_transporte1.get(i), ani_movi1.get(i),mes_movi1.get(i), 
                        dia_movi1.get(i), cla_migr1.get(i), mot_viaje1.get(i), migrante, pro_registro, can_registro, pa_registro);
                table.getItems().add(reg);
                
            }
            
       
            
        
        tip_migracion.setMinWidth(100);
        tip_nacionalidad.setMinWidth(100);
        via_transporte.setMinWidth(100);
        pro_jefmigrante.setMinWidth(100);
        can_jefmigrante.setMinWidth(100);
        ani_movi.setMinWidth(100);
        mes_movi.setMinWidth(100);
        dia_movi.setMinWidth(100);
        sex_migr.setMinWidth(100);
        ani_nacm.setMinWidth(100);
        ocu_migr.setMinWidth(100);
        cla_migr.setMinWidth(100);
        mot_viaje.setMinWidth(100);
        nac_migr.setMinWidth(100);
        pais_prosedencia .setMinWidth(100);
        pais_residencia.setMinWidth(100);
        lug_procedencia.setMinWidth(100);
        edad.setMinWidth(100);
        cont_procedencia.setMinWidth(100);
        cont_residencia.setMinWidth(100);
        cont_nacionalidad.setMinWidth(100);
        subcont_prodencia.setMinWidth(100);
        subcont_nacionalidad.setMinWidth(100);
        
        root.getChildren().addAll(label,table);
        root.setSpacing(5);
        root.setPadding(new Insets(10, 10, 10, 10));  

    }
    
    
    private void funcionGeneradora(TableColumn <RegistroMigratorio, String> valores,ArrayList<String> s,int indice ){
        valores.setCellValueFactory(cellData -> {
            RegistroMigratorio rowIndex = cellData.getValue();
            table.getItems().add(rowIndex);
            return new ReadOnlyStringWrapper(s.get(indice));
          
        });
        
    }
    
    
    private void backgroundStage(){
       Image img = new Image("Imagenes/2.jpg") ;
       stage = new StageClass(root,img);
       stage.backgroundStage(Bg);

 
    }
    
    
        

     
     private void initButton(){
        HBox hb = new HBox();
        Button eliminar = new Button("Eliminar");
        Button modificar = new Button("Modificar");
        Button registrar = new Button("Registrar");
        
        hb.getChildren().addAll(eliminar,modificar,registrar);
        hb.setSpacing(15);
        root.getChildren().add(hb);
        
       
        eliminar.setOnAction((e)->{
         RegistroMigratorio rm = table.getSelectionModel().getSelectedItem();
         PaneOrganizerModulo2.listado.eliminarRegistroMigratorio(rm);
     
        });
        
        modificar.setOnAction((e)->{
         RegistroMigratorio rm = table.getSelectionModel().getSelectedItem();
         PaneOrganizerModulo2.listado.eliminarRegistroMigratorio(rm);
        
         PaneOrganizerModulo22 pn2= new PaneOrganizerModulo22();
            Estructurasp1.scene.setRoot(pn2.getRoot());
        });
        
         registrar.setOnAction((e)->{
         
         PaneOrganizerModulo22 pn2= new PaneOrganizerModulo22();
            Estructurasp1.scene.setRoot(pn2.getRoot());
        });
        
        
     }
     
    
}