/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author johnny
 */
public class AtencionTurnosModel {
    private static PriorityQueue<Turno> cola;
    private Puesto puesto;

    public AtencionTurnosModel() {
        cola = new PriorityQueue<>(new Comparator<Turno>() {
            @Override
            public int compare(Turno o1, Turno o2) {
                char thisType = o1.getType();
                char otherType = o2.getType();
                char especial = TurnoEspecialGenerator.type;
                char terceraEdad = Turno3EdadGenerator.type;
                char embarazadas = TurnoEmbarazadasGenerator.type;
                char normal = TurnoNormalGenerator.type;
                if(thisType == especial){
                    if(otherType == especial){
                        System.out.println("this especial other especial");
                        return 0;
                    }
                    return -1;
                }
                else if(thisType == terceraEdad){
                    if(otherType == especial){
                        System.out.println("this tercera other especial");
                        return 1;
                    }
                    if(otherType == terceraEdad){
                        System.out.println("this tercera other tercera");
                        return 0;
                    }
                    System.out.println("this tercera other otros");
                    return -1;
                }
                else if(thisType == embarazadas){
                    if(otherType == especial || otherType == terceraEdad){
                        System.out.println("this embarazadas other especial o tercera edad");
                        return 1;
                    }
                    if(otherType == embarazadas){
                        System.out.println("this embarazadas other embarazadas");
                        return 0;
                    }
                    System.out.println("this embarazadas other normal");
                    return -1;
                }
                else if(thisType == normal){
                    if(otherType == normal){
                        System.out.println("this normal other normal");
                        return 0;
                    }
                    System.out.println("This normal otther otros");
                    return 1;
                }
                return -2;
            }
        });
        puesto = new Puesto(new Encargado("Johnny Beltran", "1207144278"));
    }
    
    /*
        Saca el elemento mas importante de la cola de prioridad y lo devuelve
    */
    public Turno atender(){
        return cola.poll();
    }
    
    /*
        Agrega un elemento a la cola de prioridad
    */
    public void encolar(Turno t){
        cola.offer(t);
    }
    
    public Puesto getPuesto(){
        return puesto;
    }
}
