/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author johnny
 */
public class AdminView {
    private Button addPuestoButton;
    private Button modifyPuestoButton;
    private Button deletePuestoButton;
    private Button addEncargadoButton;
    private Button modifyEncargadoButton;
    private Button deleteEncargadoButton;
    private Pane root;
    
    private static final String ADD_PUESTO = "Agregar puesto";
    private static final String MODIFY_PUESTO = "Modificar puesto";
    private static final String DELETE_PUESTO = "Eliminar puesto";
    private static final String ADD_ENCARGADO = "Agregar encargado";
    private static final String MODIFY_ENCARGADO = "Modificar encargado";
    private static final String DELETE_ENCARGADO = "Eliminar encargado";
    
    public AdminView(){
        addPuestoButton = new Button(ADD_PUESTO);
        modifyPuestoButton = new Button(MODIFY_PUESTO);
        deletePuestoButton = new Button(DELETE_PUESTO);
        addEncargadoButton = new Button(ADD_ENCARGADO);
        modifyEncargadoButton = new Button(MODIFY_ENCARGADO);
        deleteEncargadoButton = new Button(DELETE_ENCARGADO);
        
        root = new VBox(addPuestoButton, modifyPuestoButton, 
                deletePuestoButton, addEncargadoButton, modifyEncargadoButton, 
                deleteEncargadoButton);
        ((VBox)root).setAlignment(Pos.CENTER);
    }
    public Pane getRoot() {
        return root;
    }

    public Button getAddPuestoButton() {
        return addPuestoButton;
    }

    public Button getModifyPuestoButton() {
        return modifyPuestoButton;
    }

    public Button getDeletePuestoButton() {
        return deletePuestoButton;
    }

    public Button getAddEncargadoButton() {
        return addEncargadoButton;
    }

    public Button getModifyEncargadoButton() {
        return modifyEncargadoButton;
    }

    public Button getDeleteEncargadoButton() {
        return deleteEncargadoButton;
    }

    
    
    
}
