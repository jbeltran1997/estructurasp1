/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.TDAs;

import java.util.Iterator;

/**
 *
 * @author johnny
 */
public class CircularSimplyLinkedList<E> implements List<E>, Iterable<E> {
    private int efectivo;
    private Node<E> last;
    
    @Override
    public boolean isEmpty() {
        return efectivo == 0;
    }

    @Override
    public boolean addFirst(E e) {
        if(e == null)
            return false;
        Node<E> newNode = new Node<>(e);
        if(isEmpty()){
            last = newNode;
        }else{
            Node<E> first = last.getNext();
            newNode.setNext(first);
        }
        last.setNext(newNode);
        efectivo++;
        return true;
    }

    @Override
    public boolean addLast(E e) {
        if(e == null)
            return false;
        Node<E> newNode = new Node<>(e);
        if(isEmpty()){
            last = newNode;
            last.setNext(newNode);
        }else{
            Node<E> first = last.getNext();
            newNode.setNext(first);
            last.setNext(newNode);
            last = newNode;
        }
        efectivo++;
        return true;
    }

    @Override
    public boolean removeFirst() {
        if(isEmpty())
            return false;
        Node<E> first = last.getNext();
        last.setNext(first.getNext());
        first.setNext(null);
        first.setData(null);
        efectivo--;
        return true;
    }

    @Override
    public boolean removeLast() {
        if(isEmpty())
            return false;
        Node<E> previousLast = last;
        last = getPrevious(last);
        last.setNext(previousLast.getNext());
        previousLast.setData(null);
        previousLast.setNext(null);
        efectivo--;
        return true;
    }
    
    private Node<E> getPrevious(Node<E> node){
        if(node == last.getNext())
            return last;
        for(Node<E> p = last.getNext(); p != last; p = p.getNext()){
            if(p.getNext() == node)
                return p;
        }
        return null;
    }

    @Override
    public boolean contains(E e) {
        for(Node<E> p = last.getNext(); p!=last; p = p.getNext()){
            if(p.getData().equals(e))
                return true;
        }
        return false;
    }

    @Override
    public E get(int index) {
        if(index >= 0 && index < efectivo){
            int i = 0; Node<E> p;
            for(p = last.getNext(); i < index; p = p.getNext(), i++){}
            return p.getData();
        }
        return null;
    }

    @Override
    public E getFirst() {
        if(isEmpty())
            return null;
        return last.getNext().getData();
    }

    @Override
    public E getLast() {
        if(isEmpty())
            return null;
        return last.getData();
    }

    @Override
    public int size() {
        return efectivo;
    }

    @Override
    public boolean remove(int index) {
        if(index >= 0 && index < efectivo){
            if(last == last.getNext()){
                last.setData(null);
                last = null;
            }
            else{
                Node<E> p = last.getNext(); int i = 0;
                for(; i < index; p = p.getNext(), i++){}
                if(p == last)
                    removeLast();
                else if (p == last.getNext())
                    removeFirst();
                else{
                    Node<E> previous = getPrevious(p);
                    previous.setNext(p.getNext());
                    p.setNext(null);
                    p.setData(null);
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        Iterator<E> i = new Iterator<E>() {
            Node<E> p = last.getNext();
            int i = 0;
            @Override
            public boolean hasNext() {
                System.out.println((p!=null) + p.getData().toString());
                return p != null;
            }

            @Override
            public E next() {
                E e = p.getData();
                p = p.getNext();
                return e;
            }
        };
        return i;
    }
    
    
}
