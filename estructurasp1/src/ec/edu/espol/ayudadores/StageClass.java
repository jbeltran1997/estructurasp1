/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.ayudadores;


import java.time.LocalDateTime;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;


/**
 *
 * @author Nicole Alvarez
 * Clase que ayuda al manejo del fondo de casa Pane.
 */
public class StageClass {
    Pane root;
    Image img;
   
     /**
     * Constructor con parámetros
     * @param root contenedor principal.
     * @param img imágen que se asignará al fondo. 
     */
    public StageClass(Pane root,Image img){
        this.root= root;
        this.img=img; 
    }
    
    /**
     * Setea una imagen al fondo de cada pantalla.
     * @param Bg Background.
     */  
    public void backgroundStage(Background Bg){
       Bg = new Background(new BackgroundImage(img, 
              BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
            new BackgroundSize(563,352,false,false,true,true)));
       root.setBackground(Bg);
 
    }
    
    
}
