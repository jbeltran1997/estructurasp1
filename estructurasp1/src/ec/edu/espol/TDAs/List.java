/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.TDAs;

/**
 *
 * @author johnny
 */
public interface List<E> {
    boolean isEmpty();
    boolean addFirst(E e);
    boolean addLast(E e);
    boolean removeFirst();
    boolean removeLast();
    boolean contains(E e);
    E get(int index);
    E getFirst();
    E getLast();
    int size();
    boolean remove(int index);
}
