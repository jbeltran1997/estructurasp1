/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author johnny
 */
public class TurnosView {
    private static final String GENERAR = "Generar";
    private Pane root;
    private ComboBox<TurnoGenerator> tipoComboBox;
    private Button generarButton;
    public TurnosView(){
        tipoComboBox = new ComboBox(getTipos());
        generarButton = new Button(GENERAR);
        root = new VBox(tipoComboBox, generarButton);
    }
    
    private static final ObservableList<TurnoGenerator> getTipos(){
        ObservableList<TurnoGenerator> tipos = FXCollections.observableArrayList(
                new TurnoNormalGenerator(), 
                new TurnoEspecialGenerator(), 
                new Turno3EdadGenerator(),
                new TurnoEmbarazadasGenerator());
        return tipos;
        
    } 
    
    public Button getGenerarButton(){
        return generarButton;
    }
    
    public ComboBox<TurnoGenerator> getTipoComboBox(){
        return tipoComboBox;
    }

    public Pane getRoot() {
        return root;
    }
}
