/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author johnny
 */
public class ModifyEncargadoView {
    private ComboBox<Encargado> encargadoBox;
    private Button modificarButton;
    private Button eliminarButton;
    private Pane root;
    private Button backButton;
    public ModifyEncargadoView(){
        encargadoBox = new ComboBox<>();
        modificarButton = new Button("Modificar");
        eliminarButton = new Button("Eliminar");
        backButton = new Button("Atrás");
        root = new VBox(encargadoBox, modificarButton, eliminarButton, backButton);
    }
    public Button  getModButton() {
        return modificarButton;    
    }

    public Button  getDelButton() {
        return eliminarButton;
    }
    
    public Pane getRoot() {
        return root;
    }

    public ComboBox<Encargado> getEncargadoBox() {
        return encargadoBox;
    }

    public Button getBackButton() {
        return backButton;
    }
    
    
}
