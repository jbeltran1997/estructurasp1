/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import ec.edu.espol.TDAs.CircularSimplyLinkedList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

/**
 *
 * @author johnny
 */
public class PrincipalController {
    public static final String VIDEOS_FILE = "videos.txt";
    
    private AtencionTurnosModel model;
    private PrincipalView view;
    private List<String> videos;
    private CircularSimplyLinkedList<MediaPlayer> media;
    private int turnosCounter;
    private LoopingRunnable runnable;
    public PrincipalController(AtencionTurnosModel model, PrincipalView view) {
        this.model = model;
        this.view = view;
        videos = new ArrayList<>();
        media = new CircularSimplyLinkedList<>();
        turnosCounter = 1;
        
        /*runnable = new LoopingRunnable(media.iterator());
        Thread t = new Thread(runnable);
        t.start();*/
        //loopVideos(view.getMediaView(), media.iterator());
        
    }
    /*
        Carga las rutas de los videos a partir del archivo videos.txt
        Si no existe el archivo, el método lanza una excepción.
    */
    private void processVideos() throws IOException{
        BufferedReader bf = new BufferedReader(new FileReader(VIDEOS_FILE));
        String line;
        int i = 0;
        while((line = bf.readLine())!=null){
            videos.add(new File(line).toURI().toString());
        }
        bf.close();
    }
    
    public void displayTurno(Puesto p, Turno t){
        String turnoLabel = String.format("%d%20s%5c%04d", p.getNum()," ", t.getType(), t.getNum());
        if(turnosCounter > view.getTurnos().length)
            turnosCounter = 1;
        view.getTurnos()[turnosCounter++].setText(turnoLabel);
            
    }
    
    /*private void initMediaPlayer(final MediaView mediaView, 
          final Iterator<String> urls
){
    if (urls.hasNext()){
        MediaPlayer mediaPlayer = new MediaPlayer(new Media(urls.next()));
        mediaPlayer.setAutoPlay(true);
        mediaPlayer.setOnEndOfMedia(new Runnable() {
            @Override public void run() {
                initMediaPlayer(mediaView, urls);
            }
        });
        mediaView.setMediaPlayer(mediaPlayer);
    } 
}*/
    private void loopVideos(MediaView mediaView, Iterator<Media> iterator){
        if(iterator.hasNext()){
            MediaPlayer player = new MediaPlayer(iterator.next());
            player.setAutoPlay(true);
            player.setOnEndOfMedia(new Runnable() {
                @Override
                public void run() {
                    loopVideos(mediaView, iterator);
                }
            });
            mediaView.setMediaPlayer(player);
            System.out.println("finished a loop");
        }
    }
    
    public MediaView createMediaView(Iterator<Media> urls){
    MediaView mediaView = new MediaView();
    loopVideos(mediaView, urls);
    return mediaView;
    }
    
    public class LoopingRunnable implements Runnable{
        private Iterator<Media> iterator;
        public LoopingRunnable(Iterator<Media> iterator){
            this.iterator = iterator;
        }
        @Override
        public void run() {
            while(iterator.hasNext()){
            MediaPlayer player = new MediaPlayer(iterator.next());
            player.setAutoPlay(true);
            view.getMediaView().setMediaPlayer(player);
            
            }
        }
        
    }
}
