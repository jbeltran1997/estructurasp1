/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.main;

import ec.edu.espol.Modulo3.Modulo3;
import ec.edu.espol.Modulo3.Provincia;
import ec.edu.espol.ayudadores.StageClass;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Mi compu
 */
public class PaneOrganizerModulo3 {
    private VBox root = new VBox();
    private StageClass stage;
    private Background Bg;

    
    
    
    public PaneOrganizerModulo3() throws IOException {
        Modulo3 m = new Modulo3();
        
        ArrayList <Deque>arreglo = m.pilasregion();
        
        Label costa = new Label("Costa entradas: ");
        
        VBox v1 = new VBox(10);
        v1.setPadding(new Insets(10));
        v1.getChildren().add(costa);
        for(int x =0;x<6;x++){
            
            v1.getChildren().add(new Label(arreglo.get(0).pop().toString()));
            
            
        }
        
        Label sierra = new Label("Sierra entradas: ");
        
        VBox v2 = new VBox(10);
        v2.setPadding(new Insets(10));
        v2.getChildren().add(sierra);
        for(int x =0;x<11;x++){
            
            v2.getChildren().add(new Label(arreglo.get(2).pop().toString()));
            
            
        }
        
        Label oriente = new Label("Oriente entradas: " );
        VBox v3 = new VBox(10);
        v3.setPadding(new Insets(10));
        v3.getChildren().add(oriente);
        for(int x =0;x<6;x++){
            
            v3.getChildren().add(new Label(arreglo.get(4).pop().toString()));
            
            
        }
        
        Label insular = new Label("Insular entradas: ");
        VBox v4 = new VBox(10);
        v4.setPadding(new Insets(10));
        v4.getChildren().add(insular);
        for(int x =0;x<1;x++){
            
            v4.getChildren().add(new Label(arreglo.get(6).pop().toString()));
            
            
        }
        
        
        Label costa2 = new Label("Costa Salidas: ");
        VBox v5 = new VBox(10);
        v5.setPadding(new Insets(10));
        v5.getChildren().add(costa2);
        for(int x =0;x<6;x++){
            
            v5.getChildren().add(new Label(arreglo.get(1).pop().toString()));
            
            
        }
        
        Label sierra2 = new Label("Sierra Salidas: ");
        VBox v6 = new VBox(10);
        v6.setPadding(new Insets(10));
        v6.getChildren().add(sierra2);
        for(int x =0;x<11;x++){
            
            v6.getChildren().add(new Label(arreglo.get(3).pop().toString()));
            
            
        }
        Label oriente2 = new Label("Oriente Salidas: " );
        VBox v7 = new VBox(10);
        v7.getChildren().add(oriente2);
        v7.setPadding(new Insets(10));
        for(int x =0;x<6;x++){
            
            v7.getChildren().add(new Label(arreglo.get(5).pop().toString()));
            
            
        }
        
        Label insular2 = new Label("Insular Salidas: ");
        VBox v8 = new VBox(10);
        v8.setPadding(new Insets(10));
        v8.getChildren().add(insular2);
        for(int x =0;x<1;x++){
            
            v8.getChildren().add(new Label(arreglo.get(7).pop().toString()));
            
            
        }
        
        GridPane gp = new GridPane();
        gp.add(v1, 0, 0);
        gp.add(v2, 1, 0);
        gp.add(v3, 2, 0);
        gp.add(v4, 3, 0);
        gp.add(v5, 0, 1);
        gp.add(v6, 1, 1);
        gp.add(v7, 2, 1);
        gp.add(v8, 3, 1);
        
        ArrayList<ArrayList> arreglo1 = m.pilas_es();
        Label entrada = new Label("Entradas de todas la provincias: " );
        VBox v9 = new VBox(10);
        v9.setPadding(new Insets(10));
        v9.getChildren().add(entrada);
        
        for(int x =0;x<arreglo1.get(0).size();x++){
            
            Provincia p = (Provincia) arreglo1.get(0).get(x); 
            if(p.getCantidad() > 0){
            v9.getChildren().add(new Label(arreglo1.get(0).get(x).toString()));
            }
            
        }
        
        Label salida = new Label("Salidas de todas la provincias: " );
        VBox v10 = new VBox(10);
        v10.setPadding(new Insets(10));
        
        v10.getChildren().add(salida);
        
        for(int x =0;x<arreglo1.get(1).size();x++){
            
            Provincia p = (Provincia) arreglo1.get(1).get(x); 
            if(p.getCantidad() > 0){
            v10.getChildren().add(new Label(arreglo1.get(1).get(x).toString()));
            }
            
        }
        gp.add(v9, 5, 0);
        gp.add(v10, 5, 1);
        
        
        
        root.getChildren().addAll(gp);
        
        backgroundStage();
        
        
        
        
        
    }
        
        
    
    
    
    private void backgroundStage(){
       Image img = new Image("Imagenes/2.jpg") ;
       stage = new StageClass(root,img);
       stage.backgroundStage(Bg);

 
    }


    public Pane getRoot(){
        return root;
    }
    
    
}
