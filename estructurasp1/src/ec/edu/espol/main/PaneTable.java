/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.main;

import ec.edu.espol.ayudadores.StageClass;
import ec.edu.espol.ayudadores.ValoresRegistro;
import ec.edu.espol.modulo2.ListaRegistrosMigratorios;
import ec.edu.espol.modulo2.RegistroMigratorio;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author Nicole Alvarez
 */
public class PaneTable {
    private VBox root;
    private Background Bg;
    private StageClass stage;
    private int ID_pro=0;
    private String id;
    private GridPane listaPane;
    private GridPane TitulosPane;

    public PaneTable() {
        root = new  VBox();
        backgroundStage();
        crearSeccionLista();
        root.setSpacing(10);
    }
    
    private void crearSeccionLista(){
        listaPane = new GridPane();
        TitulosPane = new GridPane();
        listaPane.setGridLinesVisible(false);
        listaPane.setAlignment(Pos.CENTER);
        listaPane.setHgap(10);
        listaPane.setVgap(10);
        HBox hb = new HBox();
        Label lb = new Label("LISTA DE REGISTROS");
        lb.setFont(new Font("Berlin Sans FB", 24));
        hb.getChildren().add(lb);
        hb.setAlignment(Pos.CENTER);
        listaPane.setPadding(new Insets(30, 10, 10, 10));
        root.getChildren().add(hb);
        initTiTle();
        root.getChildren().add(TitulosPane);
        root.getChildren().add(listaPane);
        Button registrar = new Button("Registrar nuevo Migrante");
        Button c = new Button("Continuar");
        registrar.setFont(new Font("Berlin Sans FB", 16));
        root.getChildren().add(registrar);
        registrar.setOnAction((e)->{
         PaneOrganizerModulo2 pn2= new PaneOrganizerModulo2();
            Estructurasp1.scene.setRoot(pn2.getRoot());
        });
        
        c.setOnAction((e)->{
            try {
                PaneOrganizerModulo3 p3 = new PaneOrganizerModulo3();
                Estructurasp1.scene.setRoot(p3.getRoot());
            } catch (IOException ex) {
                Logger.getLogger(PaneTable.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });
        root.getChildren().add(c);
        
        int i = 1;
        
        //Se debe recorrer los contactos y por cada contactos agregar una fila al gridPane con la información del contacto
        for (RegistroMigratorio rm : PaneOrganizerModulo2.listado.getRegistros()){
            agregarFilaGrid(rm,i);
            i++;
        }
    }
    
    private void agregarFilaGrid(RegistroMigratorio rm, int i){
        rm.setId(String.valueOf(i));
        rm.getMigrante().setId_migrante(id);
        rm.getPais_procmigra().setId(id);
        rm.getCant_regmigra().setId(id);
        rm.getLug_proc().setId(id);
        Label l1=new Label(rm.getId());
        Label l2=new Label(rm.getTipo_migracion());
        Label l3=new Label(rm.getMigrante().getNacionalidad());
        Label l4=new Label(rm.getVia_transporte());
        Label l5=new Label(rm.getPais_procmigra().getNombre_pais());
        Label l6=new Label(rm.getCant_regmigra().getNombre_canton());
        Label l7=new Label(rm.getAnio_movi());
        Label l8=new Label(rm.getMes_movi());
        Label l9=new Label(rm.getDia_movi());
        Label l10=new Label(rm.getMigrante().getGenero());
        Label l11=new Label(rm.getMigrante().getAnio_nacimiento());
        Label l12=new Label(rm.getMigrante().getOcupacion());
        Label l13=new Label(rm.getClase_migra());
        Label l14=new Label(rm.getMotivo_viaje());
        Label l15=new Label(rm.getMigrante().getLugarpro().getNombre_pais());
        Label l16=new Label(rm.getMigrante().getLugarpro().getNombre_pais());
        Label l17=new Label(rm.getMigrante().getLugarres().getNombre_pais());
        Label l18=new Label(rm.getLug_proc().getNombre_pais());
        Label l19=new Label(String.valueOf(rm.getMigrante().getEdad()));
        Label l20=new Label(rm.getCont_promi().getNombre_continente());
        Label l21=new Label(rm.getCont_resmi().getNombre_continente());
        Label l22=new Label(rm.getSubcont_promi().getNombre_continente());
        Label l23=new Label(rm.getSuncont_resmi().getNombre_continente());
        int x=11;
        l1.setFont(new Font("Berlin Sans FB", x));
        l2.setFont(new Font("Berlin Sans FB", x));
        l3.setFont(new Font("Berlin Sans FB", x));
        l4.setFont(new Font("Berlin Sans FB", x));
        l5.setFont(new Font("Berlin Sans FB", x));
        l6.setFont(new Font("Berlin Sans FB", x));
        l7.setFont(new Font("Berlin Sans FB", x));
        l8.setFont(new Font("Berlin Sans FB", x));
        l9.setFont(new Font("Berlin Sans FB", x));
        l10.setFont(new Font("Berlin Sans FB", x));
        l11.setFont(new Font("Berlin Sans FB", x));
        l12.setFont(new Font("Berlin Sans FB", x));
        l13.setFont(new Font("Berlin Sans FB", x));
        l14.setFont(new Font("Berlin Sans FB", x));
        l15.setFont(new Font("Berlin Sans FB", x));
        l16.setFont(new Font("Berlin Sans FB", x));
        l17.setFont(new Font("Berlin Sans FB", x));
        l18.setFont(new Font("Berlin Sans FB", x));
        l19.setFont(new Font("Berlin Sans FB", x));
        l20.setFont(new Font("Berlin Sans FB", x));
        l21.setFont(new Font("Berlin Sans FB", x));
        l22.setFont(new Font("Berlin Sans FB", x));
        l23.setFont(new Font("Berlin Sans FB", x));
        
        Button b = new Button("Borrar");
        Button m = new Button("Modificar");
        listaPane.addRow(i+1, l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15,l16,l17,l18,l19,l20,l21,l22,l23,b,m);
        i++;

        //Eliminar
        b.setOnAction((ActionEvent e) -> {
            PaneOrganizerModulo2.listado.eliminarRegistroMigratorio(rm);
            Integer indiceFila = GridPane.getRowIndex(b);
            listaPane.getChildren().removeAll(l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15,l16,l17,l18,l19,l20,l21,l22,l23,b,m);
            for (Node child : listaPane.getChildren()) {
                Integer rowIndex = GridPane.getRowIndex(child);
                int r = rowIndex == null ? 0 : rowIndex;
                if (r > indiceFila.intValue()) {
                    GridPane.setRowIndex(child, r-1);
                }
            }
        });
        
        //Modificar
        m.setOnAction((e)->{
         PaneOrganizerModulo2.listado.eliminarRegistroMigratorio(rm);       
         PaneOrganizerModulo2 pn2= new PaneOrganizerModulo2();
            Estructurasp1.scene.setRoot(pn2.getRoot());
        });
    }
    
    private void backgroundStage(){
       Image img = new Image("Imagenes/2.jpg") ;
       stage = new StageClass(root,img);
       stage.backgroundStage(Bg);

 
    }
    
    private void initTiTle(){
        Label l1=new Label("ID");
        Label l2=new Label("Tipo_Migra");
        Label l3=new Label("Nacio");
        Label l4=new Label("Via_Trans");
        Label l5=new Label("Pais_Migra");
        Label l6=new Label("Canton_Migra");
        Label l7=new Label("Año_Migra");
        Label l8=new Label("Mes_Migra");
        Label l9=new Label("Día_Migra");
        Label l10=new Label("Genero");
        Label l11=new Label("F_Naci");
        Label l12=new Label("Ocup");
        Label l13=new Label("Cla_Migra");
        Label l14=new Label("Mot_Viaje");
        Label l15=new Label("País_Proc");
        Label l16=new Label("País_Resid");
        Label l17=new Label("Pais_Nacio");
        Label l18=new Label("Lug_Proc");
        Label l19=new Label("Edad");
        Label l20=new Label("Cont_Pro");
        Label l21=new Label("Cont_Res");
        Label l22=new Label("Subcont_Proc");
        Label l23=new Label("Subcont_Resi");
        Label b=new Label("Eliminar");
        Label m=new Label("Modificar");
        int x=11;
        l1.setFont(new Font("Berlin Sans FB", x));
        l2.setFont(new Font("Berlin Sans FB", x));
        l3.setFont(new Font("Berlin Sans FB", x));
        l4.setFont(new Font("Berlin Sans FB", x));
        l5.setFont(new Font("Berlin Sans FB", x));
        l6.setFont(new Font("Berlin Sans FB", x));
        l7.setFont(new Font("Berlin Sans FB", x));
        l8.setFont(new Font("Berlin Sans FB", x));
        l9.setFont(new Font("Berlin Sans FB", x));
        l10.setFont(new Font("Berlin Sans FB", x));
        l11.setFont(new Font("Berlin Sans FB", x));
        l12.setFont(new Font("Berlin Sans FB", x));
        l13.setFont(new Font("Berlin Sans FB", x));
        l14.setFont(new Font("Berlin Sans FB", x));
        l15.setFont(new Font("Berlin Sans FB", x));
        l16.setFont(new Font("Berlin Sans FB", x));
        l17.setFont(new Font("Berlin Sans FB", x));
        l18.setFont(new Font("Berlin Sans FB", x));
        l19.setFont(new Font("Berlin Sans FB", x));
        l20.setFont(new Font("Berlin Sans FB", x));
        l21.setFont(new Font("Berlin Sans FB", x));
        l22.setFont(new Font("Berlin Sans FB", x));
        l23.setFont(new Font("Berlin Sans FB", x));
        b.setFont(new Font("Berlin Sans FB", x));
        m.setFont(new Font("Berlin Sans FB", x));
        listaPane.addRow(0, l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15,l16,l17,l18,l19,l20,l21,l22,l23,b,m);
        
    
    
    }
    
    
    public VBox getRoot() {
        return root;
    }
    
    
    
    
    
    
    
}


