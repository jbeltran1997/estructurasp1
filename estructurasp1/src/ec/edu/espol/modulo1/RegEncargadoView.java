/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author johnny
 */
public class RegEncargadoView {
    private TextField nombre;
    private TextField cedula;
    private Button registrarButton;
    private Pane root;
    private Button backButton;
    public RegEncargadoView() {
        nombre = new TextField("Nombre completo");
        cedula = new TextField("Cédula de identidad");
        registrarButton = new Button("Registrar");
        backButton = new Button("Atrás");
        root = new VBox();
        ((VBox)root).setAlignment(Pos.CENTER);
        root.getChildren().addAll(nombre, cedula, registrarButton, backButton);
    }

    public Button getRegButton() {
        return registrarButton;
    }

    public Pane getRoot() {
        return root;
    }

    public TextField getNombre() {
        return nombre;
    }

    public TextField getCedula() {
        return cedula;
    }

    public Button getBackButton() {
        return backButton;
    }
    
    
    
}
