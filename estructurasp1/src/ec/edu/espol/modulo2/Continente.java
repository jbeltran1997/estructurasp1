/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo2;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Nicole Alvarez
 */
public class Continente {
    private final String filename = "src/archivos/Continente.txt";
    private  String nombre_continente, idcon;
    private LinkedList<Pais> paices;
    private LinkedList<RegistroMigratorio> registrosCP;
    private LinkedList<RegistroMigratorio> registrosCR;
    private LinkedList<RegistroMigratorio> registrosCN;
    private LinkedList<RegistroMigratorio> registrosSCP;
    private LinkedList<RegistroMigratorio> registrosSBR;

    public Continente(String nombre_continente,String idcon) {
        this.nombre_continente = nombre_continente;
        this.idcon= idcon;
        registrosCN = new LinkedList<>();
        registrosCP = new LinkedList<>();
        registrosCR = new LinkedList<>();
        registrosSBR = new LinkedList<>();
        registrosSCP = new LinkedList<>();
        paices = new LinkedList<>();
        
    }
    
    public Continente(String nombre_continente,LinkedList<Pais> paices) {
        this.nombre_continente = nombre_continente;
        this.idcon=null;
        this.paices=paices;
      
    }
    
   public void agregarPais(Pais p){
       if(p!=null&&!paices.contains(p)){
           paices.add(p);
       } 
   }
   
   public void escribirContinente(){
       try(BufferedWriter outputStream =
                 new BufferedWriter(new FileWriter(filename,true)))
        {
                
                outputStream.write(nombre_continente+","+idcon);
                outputStream.newLine();
                outputStream.close();
            
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error opening the file out.txt."+ e.getMessage());
        }
        catch(IOException e){
            System.out.println("IOException."+ e.getMessage());
        }
   
   
   }

    public LinkedList<Pais> getPaices() {
        return paices;
    }

    public void setPaices(LinkedList<Pais> paices) {
        this.paices = paices;
    }

    public String getNombre_continente() {
        return nombre_continente;
    }

    public void setNombre_continente(String nombre_continente) {
        this.nombre_continente = nombre_continente;
    }

    public String getIdcon() {
        return idcon;
    }

    public void setIdcon(String idcon) {
        this.idcon = idcon;
    }

    
    

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Continente other = (Continente) obj;
        if (!Objects.equals(this.nombre_continente, other.nombre_continente)) {
            return false;
        }
        if (!Objects.equals(this.idcon, other.idcon)) {
            return false;
        }
        return true;
    }

    
    

    public LinkedList<RegistroMigratorio> getRegistrosCP() {
        return registrosCP;
    }

    public void setRegistrosCP(LinkedList<RegistroMigratorio> registrosCP) {
        this.registrosCP = registrosCP;
    }

    public LinkedList<RegistroMigratorio> getRegistrosCR() {
        return registrosCR;
    }

    public void setRegistrosCR(LinkedList<RegistroMigratorio> registrosCR) {
        this.registrosCR = registrosCR;
    }

    public LinkedList<RegistroMigratorio> getRegistrosSCP() {
        return registrosSCP;
    }

    public void setRegistrosSCP(LinkedList<RegistroMigratorio> registrosSCP) {
        this.registrosSCP = registrosSCP;
    }

    public LinkedList<RegistroMigratorio> getRegistrosSBR() {
        return registrosSBR;
    }

    public void setRegistrosSBR(LinkedList<RegistroMigratorio> registrosSBR) {
        this.registrosSBR = registrosSBR;
    }

    public LinkedList<RegistroMigratorio> getRegistrosCN() {
        return registrosCN;
    }

    public void setRegistrosCN(LinkedList<RegistroMigratorio> registrosCN) {
        this.registrosCN = registrosCN;
    }

    @Override
    public String toString() {
        return nombre_continente;
    }
    
    
   
   
   
}
