/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.ayudadores;

import ec.edu.espol.modulo2.Canton;
import ec.edu.espol.modulo2.Continente;
import ec.edu.espol.modulo2.Pais;
import ec.edu.espol.modulo2.Provincia;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author Nicole Alvarez
 */
public class ValoresRegistro {
    private static LinkedList<Provincia> p;
    private static LinkedList<Continente> c;
    private static LinkedList<Pais> pa;
    public ValoresRegistro() {
        CrearListaProvincias();
        CrearListaContinente();
    }
    
    public static ArrayList<String> asignarPais(String continente){
        LinkedList<Pais> ca = new LinkedList<>();
        ArrayList<String> conti = new ArrayList<>();
        for(Continente cn:c){
            if(cn.getNombre_continente().equals(continente))
                ca = cn.getPaices();
        
        }
        Iterator i = ca.listIterator();
        while(i.hasNext()){
            Pais cam =(Pais)i.next();
            conti.add(cam.getNombre_pais());
        }
        return conti;
    
    }
    
    public static ArrayList<String> asignarsubContienete(String contiente){
        ArrayList<String> subconti = new ArrayList<>();
        if(contiente.equals("America")){
            subconti.add("América del Norte");
            subconti.add("Centro América");
            subconti.add("América del Sur");
        }
        else
           subconti.add(contiente);
        return subconti;
    
    }
    
    
    
    
    public static ArrayList<String> asignarProvinica(String paiss){
        LinkedList<Provincia> prov = new LinkedList<>();
        ArrayList<String> provincias = new ArrayList<>();
        for(Pais pais:pa){
            if(pais.getNombre_pais().equals(paiss))
                prov = pais.getProvincias();
        
        }
        Iterator i = prov.listIterator();
        while(i.hasNext()){
            Provincia pr =(Provincia)i.next();
            provincias.add(pr.getNombre_provincia());
        }
        return provincias;
    
    }
    
    
    
    
    
    
    
    public static ArrayList<String> asignarCanton(String n_pro){
        LinkedList<Canton> ca = new LinkedList<>();
        ArrayList<String> c = new ArrayList<>();
        for(Provincia pr : p){
            if(pr.getNombre_provincia().equals(n_pro)){
                ca = pr.getCantones();
            }
    
        }
        Iterator i = ca.listIterator();
        while(i.hasNext()){
            Canton cam =(Canton)i.next();
            c.add(cam.getNombre_canton());
        }
        return c;
    
    }
    
    public static ArrayList<String> Provincias(){
        ArrayList<String> pro = new ArrayList<String>();
        Iterator i = p.listIterator();
        while(i.hasNext()){
            Provincia prov =(Provincia)i.next();
            pro.add(prov.getNombre_provincia());
        }
        return pro;
    
    }
    
    
     public static ArrayList<String> Continentes(){
        ArrayList<String> con = new ArrayList<String>();
        Iterator i = c.listIterator();
        while(i.hasNext()){
            Continente cont =(Continente)i.next();
            con.add(cont.getNombre_continente());
        }
        return con;
    
    }

    
    private void CrearListaContinente(){
       
       c = new LinkedList<>();
       LinkedList<Pais> paisameri = new LinkedList<>();
      paisameri.add(new Pais("Antigua y Barbuda"));
       paisameri.add(new Pais("Argentina"));
       paisameri.add(new Pais("Bahamas"));
       paisameri.add(new Pais("Barbados"));
       paisameri.add(new Pais("Belice"));
       paisameri.add(new Pais("Bolivia"));
       paisameri.add(new Pais("Brasil"));
       paisameri.add(new Pais("Canadá"));
       paisameri.add(new Pais("Chile"));
       paisameri.add(new Pais("Colombia"));
       paisameri.add(new Pais("Costa Rica"));
       paisameri.add(new Pais("Cuba"));
       paisameri.add(new Pais("Domínica"));
       paisameri.add(new Pais("Ecuador"));
       paisameri.add(new Pais("El Salvador"));
       paisameri.add(new Pais("Estados Unidos"));
       paisameri.add(new Pais("Granada"));
       paisameri.add(new Pais("Guatemala"));
       paisameri.add(new Pais("Guyana"));
       paisameri.add(new Pais("Haití"));
       paisameri.add(new Pais("Honduras"));
       paisameri.add(new Pais("Jamaica"));
       paisameri.add(new Pais("México"));
       paisameri.add(new Pais("Nicaragua"));
       paisameri.add(new Pais("Panamá"));
       paisameri.add(new Pais("Paraguay"));
       paisameri.add(new Pais("Perú"));
       paisameri.add(new Pais("República Dominicana"));
       paisameri.add(new Pais("San Cristobal y Nieves"));
       paisameri.add(new Pais("San Vicente y las Granadinas"));
       paisameri.add(new Pais("Santa Lucia"));
       paisameri.add(new Pais("Surinam"));
       paisameri.add(new Pais("Trinidad y Tobago"));
       paisameri.add(new Pais("Uruguay"));
       paisameri.add(new Pais("Venezuela"));
       c.add(new Continente("America",paisameri));
      
       
       LinkedList<Pais> paisEuropa = new LinkedList<>();
      paisEuropa.add(new Pais("Albania"));
       paisEuropa.add(new Pais("Alemania"));
       paisEuropa.add(new Pais("Andorra"));
       paisEuropa.add(new Pais("Armenia"));
       paisEuropa.add(new Pais("Austria"));
       paisEuropa.add(new Pais("Azerbaiyán"));
       paisEuropa.add(new Pais("Bélgica"));
       paisEuropa.add(new Pais("Bielorrusia"));
       paisEuropa.add(new Pais("Bosnia y Herzegovina"));
       paisEuropa.add(new Pais("Bulgaria"));
       paisEuropa.add(new Pais("Chipre"));
       paisEuropa.add(new Pais("Ciudad del Vaticano"));
       paisEuropa.add(new Pais("Croacia"));
       paisEuropa.add(new Pais("Dinamarca"));
       paisEuropa.add(new Pais("Eslovaquia"));
       paisEuropa.add(new Pais("Eslovenia"));
       paisEuropa.add(new Pais("España"));
       paisEuropa.add(new Pais("Estonia"));
       paisEuropa.add(new Pais("Finlandia"));
       paisEuropa.add(new Pais("Francia"));
       paisEuropa.add(new Pais("Georgia"));
       paisEuropa.add(new Pais("Grecia"));
       paisEuropa.add(new Pais("Hungría"));
       paisEuropa.add(new Pais("Irlanda"));
       paisEuropa.add(new Pais("Islandia"));
       paisEuropa.add(new Pais("Italia"));
       paisEuropa.add(new Pais("Kazajistán"));
       paisEuropa.add(new Pais("Letonia"));
       paisEuropa.add(new Pais("Liechtenstein"));
       paisEuropa.add(new Pais("Lituania"));
       paisEuropa.add(new Pais("Luxemburgo"));
       paisEuropa.add(new Pais("Macedonia"));
       paisEuropa.add(new Pais("Malta"));
       paisEuropa.add(new Pais("Maldovia"));
       paisEuropa.add(new Pais("Mónaco"));
       paisEuropa.add(new Pais("Montenegro"));
       paisEuropa.add(new Pais("Noruega"));
       paisEuropa.add(new Pais("Paises Bajos"));
       paisEuropa.add(new Pais("Polonia"));
       paisEuropa.add(new Pais("Portugal"));
       paisEuropa.add(new Pais("Reino Unido"));
       paisEuropa.add(new Pais("República Checa"));
       paisEuropa.add(new Pais("Rumania"));
       paisEuropa.add(new Pais("Rusia"));
       paisEuropa.add(new Pais("San Marino"));
       paisEuropa.add(new Pais("Serbia"));
       paisEuropa.add(new Pais("Suecia"));
       paisEuropa.add(new Pais("Serbia"));
       paisEuropa.add(new Pais("Suiza"));
       paisEuropa.add(new Pais("Turquía"));
       paisEuropa.add(new Pais("Rumania"));
       c.add(new Continente("Europa",paisEuropa));
       
      
       
       LinkedList<Pais> paisasia = new LinkedList<>();
       paisasia.add(new Pais("Afganistán"));paisasia.add(new Pais("Arabia Saudita"));paisasia.add(new Pais("Armenia"));
       paisasia.add(new Pais("Azerbaiyán"));paisasia.add(new Pais("Bahréin"));paisasia.add(new Pais("Bangladés"));
       paisasia.add(new Pais("Birmania"));paisasia.add(new Pais("Bután"));paisasia.add(new Pais("Burnei"));
       paisasia.add(new Pais("Camboya"));paisasia.add(new Pais("Caltar"));paisasia.add(new Pais("Chipre"));paisasia.add(new Pais("China"));paisasia.add(new Pais("Corea del Norte"));
       paisasia.add(new Pais("Corea del Sur"));paisasia.add(new Pais("Emiratos Árabes Unidos"));paisasia.add(new Pais("Filipinas"));
       paisasia.add(new Pais("Georgia"));paisasia.add(new Pais("Indonesia"));paisasia.add(new Pais("India"));paisasia.add(new Pais("Irán"));
       paisasia.add(new Pais("Israel"));paisasia.add(new Pais("Japón"));paisasia.add(new Pais("Jordania"));paisasia.add(new Pais("Kazajistán"));
       paisasia.add(new Pais("Kirguistán"));paisasia.add(new Pais("Kuwait"));paisasia.add(new Pais("Laos"));paisasia.add(new Pais("Líbano"));
       paisasia.add(new Pais("Malasia"));paisasia.add(new Pais("Maldiva"));paisasia.add(new Pais("Mongolia"));paisasia.add(new Pais("Nepal"));
       paisasia.add(new Pais("Omán"));paisasia.add(new Pais("Pakistán"));paisasia.add(new Pais("Catar"));paisasia.add(new Pais("Rusia"));
       paisasia.add(new Pais("Singapur"));paisasia.add(new Pais("Siria"));paisasia.add(new Pais("Sri Lanka"));paisasia.add(new Pais("Tailandia"));
       paisasia.add(new Pais("Tayikistán"));paisasia.add(new Pais("Timor Oriental"));paisasia.add(new Pais("Turkmenistán"));paisasia.add(new Pais("Turquía"));
       paisasia.add(new Pais("Uzbekistán"));paisasia.add(new Pais("Vietnam"));paisasia.add(new Pais("Yemen"));
       c.add(new Continente("Asia",paisasia));
       

       LinkedList<Pais> paisoceania = new LinkedList<>();
       paisoceania.add(new Pais("Australia"));paisoceania.add(new Pais("Estados Federados de Micronesia"));
       paisoceania.add(new Pais("Fiji"));paisoceania.add(new Pais("Islas Marshall"));
       paisoceania.add(new Pais("Islas Salomón"));paisoceania.add(new Pais("Kiribati"));
       paisoceania.add(new Pais("Micronesia"));paisoceania.add(new Pais("Nauru"));
       paisoceania.add(new Pais("Nueva Zelanda"));paisoceania.add(new Pais("Palau"));
       paisoceania.add(new Pais("Papúa Nueva Guinea"));paisoceania.add(new Pais("Samoa"));
       paisoceania.add(new Pais("Timor Oriental"));paisoceania.add(new Pais("Tonga"));
       paisoceania.add(new Pais("Tuvalu"));paisoceania.add(new Pais("Vanuatu"));

       c.add(new Continente("Oceania",paisoceania));
       
     
       LinkedList<Pais> paiafrica = new LinkedList<>();
       paiafrica.add(new Pais("Angola"));paiafrica.add(new Pais("Argelia"));paiafrica.add(new Pais("Benin"));
       paiafrica.add(new Pais("Bostwana"));paiafrica.add(new Pais("Burkina Fasso"));paiafrica.add(new Pais("Burundi"));paiafrica.add(new Pais("Camerún"));
       paiafrica.add(new Pais("Chad"));paiafrica.add(new Pais("Costa de Marfil"));paiafrica.add(new Pais("Egipto"));
       paiafrica.add(new Pais("El Congo"));paiafrica.add(new Pais("Eritrea"));paiafrica.add(new Pais("Etiopía"));
       paiafrica.add(new Pais("Gabón"));;paiafrica.add(new Pais("Gambia"));paiafrica.add(new Pais("Ghana"));paiafrica.add(new Pais("Guinea"));paiafrica.add(new Pais("Guinea Ecuatorial"));
       paiafrica.add(new Pais("Guinea-Bissau"));paiafrica.add(new Pais("Kenia"));paiafrica.add(new Pais("Lesotho"));
       paiafrica.add(new Pais("Liberia"));paiafrica.add(new Pais("Libia"));paiafrica.add(new Pais("Madagascar"));
       paiafrica.add(new Pais("Malawi"));paiafrica.add(new Pais("Marruecos"));paiafrica.add(new Pais("Mauritania"));
       paiafrica.add(new Pais("Mozambique"));paiafrica.add(new Pais("Namibia"));paiafrica.add(new Pais("Níger"));
       paiafrica.add(new Pais("República Democrática del Congo"));paiafrica.add(new Pais("Ruanda"));paiafrica.add(new Pais("Sahara Occidental"));
       paiafrica.add(new Pais("Sudán"));paiafrica.add(new Pais("Sudán del Sur"));paiafrica.add(new Pais("Swazilandia"));
       paiafrica.add(new Pais("Tanzania"));paiafrica.add(new Pais("Togo"));paiafrica.add(new Pais("Uganda"));
       paiafrica.add(new Pais("Yibuti"));paiafrica.add(new Pais("Zambia"));paiafrica.add(new Pais("Zimbabwe"));
       c.add(new Continente("África",paiafrica));
     
    }
    
    
    private void CrearListaProvincias(){
        p = new LinkedList<>();
        LinkedList<Canton> ca = new LinkedList<>();
        LinkedList<Canton> cb = new LinkedList<>();
        LinkedList<Canton> cni = new LinkedList<>();
        LinkedList<Canton> car = new LinkedList<>();
        LinkedList<Canton> cchim = new LinkedList<>();
        LinkedList<Canton> ccot = new LinkedList<>();
        LinkedList<Canton> coro = new LinkedList<>();
        LinkedList<Canton> cesme = new LinkedList<>();
        LinkedList<Canton> cgala = new LinkedList<>();
        LinkedList<Canton> cgua = new LinkedList<>();
        LinkedList<Canton> cimba = new LinkedList<>();
        LinkedList<Canton> clo = new LinkedList<>();
        LinkedList<Canton> clos = new LinkedList<>();
        LinkedList<Canton> cman = new LinkedList<>();
        LinkedList<Canton> cmo = new LinkedList<>();
        LinkedList<Canton> cna = new LinkedList<>();
        LinkedList<Canton> cor = new LinkedList<>();
        LinkedList<Canton> cpas = new LinkedList<>();
        LinkedList<Canton> cpic = new LinkedList<>();
        LinkedList<Canton> csa = new LinkedList<>();
        LinkedList<Canton> csanto = new LinkedList<>();
        LinkedList<Canton> csuc = new LinkedList<>();
        LinkedList<Canton> cza = new LinkedList<>();
        
        
        ca.add(new Canton("Cuenca"));
        cb.add(new Canton("Guaranda"));
        cni.add(new Canton("Azogues"));
        car.add(new Canton("Tulcán"));
        cchim.add(new Canton("Riobamba"));
        ccot.add(new Canton("Latacunga"));
        coro.add(new Canton("Machala"));
        cesme.add(new Canton("Esmeraldas"));
        cgala.add(new Canton("Puerto Baquerizo Moreno"));
        cgua.add(new Canton("Guayaquil"));
        cimba.add(new Canton("Ibarra"));
        clo.add(new Canton("Loja"));
        clos.add(new Canton("Babahoyo"));
        cman.add(new Canton("Portoviejo"));
        cmo.add(new Canton("Macas"));
        cna.add(new Canton("Tena"));
        cor.add(new Canton("Francisco de Orellana"));
        cpas.add(new Canton("Puyo"));
        cpic.add(new Canton("Quito"));
        csa.add(new Canton("Santa Elena"));
        csanto.add(new Canton("Santo Domingo"));
        csuc.add(new Canton("Nueva Loja"));
        cza.add(new Canton(" Zamora"));
        
        
        p.add(new Provincia("Azuay",ca));
        p.add(new Provincia("Bolívar",cb));
        p.add(new Provincia("Cañar",cni));
        p.add(new Provincia("Carchi",car));
        p.add(new Provincia("Chimborazo",cchim));
        p.add(new Provincia("Cotopaxi",ccot));
        p.add(new Provincia("El Oro",coro));
        p.add(new Provincia("Esmeraldas",cesme));
        p.add(new Provincia("Galápagos",cgala));
        p.add(new Provincia("Guayas",cgua));
        p.add(new Provincia("Imbabura",cimba));
        p.add(new Provincia("Loja",clo));
        p.add(new Provincia("Los Ríos",clos));
        p.add(new Provincia("Manabí",cman));
        p.add(new Provincia("Morona Santiago",cmo));
        p.add(new Provincia("Napo",cna));
        p.add(new Provincia("Orellana",cor));
        p.add(new Provincia("Pastaza",cpas));
        p.add(new Provincia("Pichincha",cpic));
        p.add(new Provincia("Santa Elena",csa));
        p.add(new Provincia("Santo Domingo de los Tsáchilas",csanto));
        p.add(new Provincia("Sucumbíos",csuc));
        p.add(new Provincia("Zamora Chinchipe",cza));

    
    }
}
