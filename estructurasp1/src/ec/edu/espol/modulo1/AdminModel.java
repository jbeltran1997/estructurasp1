/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author johnny
 */
public class AdminModel {
    private List<Puesto> puestos;
    private List<Encargado> encargados;

    public AdminModel() {
        this.puestos = new ArrayList<>();
        this.encargados = new ArrayList<>();
    }
    
    /*
        Añade un puesto a la lista de puestos
    */
    public void registrarPuesto(Puesto p){
        puestos.add(p);
    }
    
    /*
        Modifica un puesto de la lista de puestos
    */
    public void modificar(Puesto p, Encargado e){
        //p.setEncargado(e);
    }
    
    /*
        Elimina un puesto de la lista de puestos
    */
    public void eliminarPuesto(Puesto p){
        puestos.remove(p);
    }
    
    /*
        Añade un encargado a la lista de encargados
    */
    public void registrarEncargado(Encargado e){
        encargados.add(e);
    }

    /*
        Elimina un encargado de la lista de encargados
    */
    public void eliminarEncargado(Encargado e){
        encargados.remove(e);
    }

    public List<Puesto> getPuestos() {
        return puestos;
    }

    public List<Encargado> getEncargados() {
        return encargados;
    }
    
    
}
