/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import java.io.File;
import java.net.MalformedURLException;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author johnny
 */
public class PuestoView {
    private Pane root;
    private Button atenderButton;
    public static final String ATENDER = "Atender";
    public PuestoView(){
        root = new VBox();
        atenderButton = new Button(ATENDER);
        root.getChildren().add(atenderButton);
        try{
        Image img = new Image(new File("src/Imagenes/2.jpg").toURI().toURL().toString());
            System.out.println(new File("src/Imagenes/2.jpg").toURI().toURL().toString());
        BackgroundImage bi = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT, 
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, 
                BackgroundSize.DEFAULT);
        root.setBackground(new Background(bi));
        }catch(MalformedURLException e){}
    }
    public Button getAtenderButton(){
        return atenderButton;
    }

    public Pane getRoot() {
        return root;
    }
}
