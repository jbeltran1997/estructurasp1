/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo2;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Nicole Alvarez
 */
public class Migrante{
    private String filename = "src/archivos/Migrante.txt";
    private String id_migrante,nacionalidad,genero,anio_nacimiento ,ocupacion;
    private Pais lugarpro;
    private Pais lugarres;
    private Continente cont_pro;
    private Continente cont_res;
    private Continente continen_nac;
    private Continente subcont_pro;
    private Continente suncont_res;
    private int edad;
    private LinkedList<RegistroMigratorio> registro;
    private LinkedList<Canton> registroCantones;
    private LinkedList<Provincia> registroProvincia;
    private LinkedList<Pais> registroPaisRes;
    private LinkedList<Continente> registrContinenteRes;

    public Migrante(String id_migrante, String nacionalidad, 
            String genero, String anio_nacimiento, String ocupacion, 
            Pais lugarpro, Pais lugarres, Continente cont_pro, Continente cont_res, Continente continen_nac, Continente subcont_pro, Continente suncont_res) {
        this.id_migrante = id_migrante;
        this.nacionalidad = nacionalidad;
        this.genero = genero;
        this.anio_nacimiento = anio_nacimiento;
        this.ocupacion = ocupacion;
        this.lugarpro = lugarpro;
        this.lugarres = lugarres;
        this.cont_pro = cont_pro;
        this.cont_res = cont_res;
        this.continen_nac = continen_nac;
        this.subcont_pro = subcont_pro;
        this.suncont_res = suncont_res;
        //calcularedad();
        registrContinenteRes = new LinkedList<>();
        registro = new LinkedList<>();
        registroPaisRes = new LinkedList<>();
        registroProvincia = new LinkedList<>();
        registroCantones = new LinkedList<>();
        calcularedad();
        
    }

   
    public void escribirMigrante(){
       try(BufferedWriter outputStream =
                 new BufferedWriter(new FileWriter(filename,true)))
        {
                outputStream.write(id_migrante+","+nacionalidad+","+genero+","+anio_nacimiento+","+ocupacion+","+lugarpro+","+String.valueOf(edad));
                outputStream.newLine();
                outputStream.close();
            
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error opening the file out.txt."+ e.getMessage());
        }
        catch(IOException e){
            System.out.println("IOException."+ e.getMessage());
        }
   
   }
    
   
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getId_migrante() {
        return id_migrante;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public String getGenero() {
        return genero;
    }

    public String getAnio_nacimiento() {
        return anio_nacimiento;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setId_migrante(String id_migrante) {
        this.id_migrante = id_migrante;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setAnio_nacimiento(String anio_nacimiento) {
        this.anio_nacimiento = anio_nacimiento;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }
    
    
   
   

    public Pais getLugarpro() {
        return lugarpro;
    }

    public void setLugarpro(Pais lugarpro) {
        this.lugarpro = lugarpro;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public LinkedList<RegistroMigratorio> getRegistro() {
        return registro;
    }

    public void setRegistro(LinkedList<RegistroMigratorio> registro) {
        this.registro = registro;
    }

    public Pais getLugarres() {
        return lugarres;
    }

    public void setLugarres(Pais lugarres) {
        this.lugarres = lugarres;
    }

    

    public Continente getCont_pro() {
        return cont_pro;
    }

    public void setCont_pro(Continente cont_pro) {
        this.cont_pro = cont_pro;
    }

    public Continente getCont_res() {
        return cont_res;
    }

    public void setCont_res(Continente cont_res) {
        this.cont_res = cont_res;
    }

    public Continente getContinen_nac() {
        return continen_nac;
    }

    public void setContinen_nac(Continente continen_nac) {
        this.continen_nac = continen_nac;
    }

    public Continente getSubcont_pro() {
        return subcont_pro;
    }

    public void setSubcont_pro(Continente subcont_pro) {
        this.subcont_pro = subcont_pro;
    }

    public Continente getSuncont_res() {
        return suncont_res;
    }

    public void setSuncont_res(Continente suncont_res) {
        this.suncont_res = suncont_res;
    }

   

    public LinkedList<Canton> getRegistroCantones() {
        return registroCantones;
    }

    public void setRegistroCantones(LinkedList<Canton> registroCantones) {
        this.registroCantones = registroCantones;
    }

    public LinkedList<Provincia> getRegistroProvincia() {
        return registroProvincia;
    }

    public void setRegistroProvincia(LinkedList<Provincia> registroProvincia) {
        this.registroProvincia = registroProvincia;
    }

    public LinkedList<Pais> getRegistroPaisRes() {
        return registroPaisRes;
    }

    public void setRegistroPaisRes(LinkedList<Pais> registroPaisRes) {
        this.registroPaisRes = registroPaisRes;
    }

    public LinkedList<Continente> getRegistrContinenteRes() {
        return registrContinenteRes;
    }

    public void setRegistrContinenteRes(LinkedList<Continente> registrContinenteRes) {
        this.registrContinenteRes = registrContinenteRes;
    }
    
    private void calcularedad(){
        int anio=0;
        if(anio_nacimiento!=null){
        anio = Integer.parseInt(anio_nacimiento);}
        edad = 2018 - anio;
    }

    @Override
    public String toString() {
        return "Migrante{" + "id_migrante=" + id_migrante + ", nacionalidad=" + nacionalidad + ", genero=" + genero + ", anio_nacimiento=" + anio_nacimiento + ", ocupacion=" + ocupacion + ", lugarpro=" + lugarpro + ", lugarres=" + lugarres + ", cont_pro=" + cont_pro + ", cont_res=" + cont_res + ", continen_nac=" + continen_nac + ", subcont_pro=" + subcont_pro + ", suncont_res=" + suncont_res + ", edad=" + edad + ", registro=" + registro + ", registroCantones=" + registroCantones + ", registroProvincia=" + registroProvincia + ", registroPaisRes=" + registroPaisRes + ", registrContinenteRes=" + registrContinenteRes + '}';
    }
    
    
    
    
    
    
    
}
