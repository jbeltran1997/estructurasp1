/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modulo1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author johnny
 */
public class TestModulo1 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        
        AtencionTurnosModel atencion = new AtencionTurnosModel();
        PrincipalView principalView = new PrincipalView();
        
        PrincipalController principalController = new PrincipalController(atencion, principalView);
        Pane root = principalView.getRoot();
        
        Scene scene = new Scene(root, 1280, 720);
        
        
        primaryStage.setTitle("PrincipalView");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        PuestoView puestoView = new PuestoView();
        PuestoController puestoController = new PuestoController(atencion, puestoView, principalController);
        
        TurnosView generateView = new TurnosView();
        TurnosController turnosController = new TurnosController(atencion, generateView);
        
        AdminModel adminModel = new AdminModel();
        adminModel.getPuestos().add(atencion.getPuesto());
        adminModel.getEncargados().add(atencion.getPuesto().getEncargado());
        AdminView adminView = new AdminView();
        RegEncargadoView regEView = new RegEncargadoView();
        RegPuestoView regPView = new RegPuestoView();
        ModifyEncargadoView modEView = new ModifyEncargadoView();
        ModifyPuestoView modPView = new ModifyPuestoView();
        AdminController adminController = new AdminController(adminModel, regEView, regPView, modEView, modPView, adminView);
        
        Stage generateStage = new Stage();
        Stage puestoStage = new Stage();
        Stage adminStage = new Stage();
        
        Scene generateScene = new Scene(generateView.getRoot(), 800, 800);
        Scene puestoScene = new Scene(puestoView.getRoot(), 800, 800);
        Scene adminScene = new Scene(adminView.getRoot(), 800,800);
        
        generateStage.setScene(generateScene);
        puestoStage.setScene(puestoScene);
        adminStage.setScene(adminScene);
        
        generateStage.show();
        puestoStage.show();
        adminStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
